import { useDispatch, useSelector } from "react-redux";
import { useCallback } from "react";
import { addTagWord, addTagWordList } from "reducers/tagWord";
import { RootState } from "store";

export function useTagWords() {
  const tagWords = useSelector((state: RootState) => state.tagWords);
  return tagWords;
}

export function useAddTagWord() {
  const dispatch = useDispatch();
  return useCallback((tagword) => dispatch(addTagWord(tagword)), [dispatch]);
}

export function useAddTagWordList() {
  const dispatch = useDispatch();
  return useCallback(
    (tagWords) => dispatch(addTagWordList(tagWords)),
    [dispatch]
  );
}
