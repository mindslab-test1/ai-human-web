import { useDispatch, useSelector } from "react-redux";
import React, { useCallback } from "react";
import { muteVideo, unmuteVideo } from "reducers/muteButton";
import { RootState } from "store";

export function useMuteVideo() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(muteVideo()), [dispatch]);
}

export function useUnmuteVideo() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(unmuteVideo()), [dispatch]);
}
