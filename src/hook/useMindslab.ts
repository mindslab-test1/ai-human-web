import { useDispatch } from "react-redux";
import { useCallback } from "react";
import { setMindslab, unsetMindslab } from "reducers/isMindslab";

export function useSetMindslab() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(setMindslab()), [dispatch]);
}

export function useUnsetMindslab() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(unsetMindslab()), [dispatch]);
}
