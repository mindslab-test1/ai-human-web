import { useDispatch, useSelector } from "react-redux";
import { useCallback } from "react";
import { addChat, addChatList } from "reducers/chatBoxs";
import { RootState } from "store";

export function useChatBoxs() {
  const chatBoxs = useSelector((state: RootState) => state.chatBoxs);
  return chatBoxs;
}

export function useAddChat() {
  const dispatch = useDispatch();
  return useCallback((chatBox) => dispatch(addChat(chatBox)), [dispatch]);
}

export function useAddChatList() {
  const dispatch = useDispatch();
  return useCallback(
    (chatState) => dispatch(addChatList(chatState)),
    [dispatch]
  );
}
