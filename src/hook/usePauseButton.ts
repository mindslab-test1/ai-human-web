import { useDispatch, useSelector } from "react-redux";
import React, { useCallback } from "react";
import {
  pauseVideo,
  playVideo,
  tempPauseVideo,
  nTempPauseVideo,
} from "reducers/pauseButton";
import { RootState } from "store";

export function usePauseButton() {
  const pauseButton = useSelector((state: RootState) => state.pauseButton);
  return pauseButton;
}

export function usePauseVideo() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(pauseVideo()), [dispatch]);
}

export function usePlayVideo() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(playVideo()), [dispatch]);
}

export function useTmepPauseVideo() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(tempPauseVideo()), [dispatch]);
}

export function useNTempPauseVideo() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(nTempPauseVideo()), [dispatch]);
}
