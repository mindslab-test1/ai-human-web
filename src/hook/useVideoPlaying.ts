import { useDispatch } from "react-redux";
import { useCallback } from "react";
import { videoPlaying, videoNotPlaying } from "reducers/isVideoPlaying";

export function useVideoPlaying() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(videoPlaying()), [dispatch]);
}

export function useVideoNotPlaying() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(videoNotPlaying()), [dispatch]);
}
