import { useDispatch, useSelector } from "react-redux";
import { useCallback } from "react";
import {
  setInit,
  setInitialVideoSrc,
  setLoadedVideoSrc,
  muteVideo,
  unMuteVideo,
  pauseVideo,
  playVideo,
  tempPause,
  notTempPause,
} from "reducers/videoElement";
import { RootState } from "store";

export function useVideoElement() {
  const videoElement = useSelector((state: RootState) => state.videoElement);
  return videoElement;
}

export function useSetInit() {
  const dispatch = useDispatch();
  return useCallback((videoState) => dispatch(setInit(videoState)), [dispatch]);
}

export function useSetInitialVideoSrc() {
  const dispatch = useDispatch();
  return useCallback(
    (videoSrc) => dispatch(setInitialVideoSrc(videoSrc)),
    [dispatch]
  );
}

export function useSetLoadedVideoSrc() {
  const dispatch = useDispatch();
  return useCallback(
    (videoSrc) => dispatch(setLoadedVideoSrc(videoSrc)),
    [dispatch]
  );
}

export function useMuteVideoElement() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(muteVideo()), [dispatch]);
}

export function useUnMuteVideoElement() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(unMuteVideo()), [dispatch]);
}

export function usePlayVideoElement() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(playVideo()), [dispatch]);
}

export function usePauseVideoElement() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(pauseVideo()), [dispatch]);
}

export function useTempPauseElement() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(tempPause()), [dispatch]);
}

export function useNotTempPauseElement() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(notTempPause()), [dispatch]);
}
