import { useDispatch, useSelector } from "react-redux";
import { useCallback } from "react";
import {
  addSaveQuestion,
  openChat,
  closeChat,
  sendSaveQuestion,
} from "reducers/saveQuestion";
import { RootState } from "store";

export function useSaveQuestion() {
  const saveQuestion = useSelector((state: RootState) => state.saveQuestion);
  return saveQuestion;
}

export function useAddSaveQuestion() {
  const dispatch = useDispatch();
  return useCallback(
    (saveChat) => dispatch(addSaveQuestion(saveChat)),
    [dispatch]
  );
}

export function useOpenChat() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(openChat()), [dispatch]);
}

export function useCloseChat() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(closeChat()), [dispatch]);
}

export function useSendSavedQuestion() {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(sendSaveQuestion()), [dispatch]);
}
