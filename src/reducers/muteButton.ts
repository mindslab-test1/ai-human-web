const MUTE_VIDEO = "muteButton/MUTE_VIDEO" as const;
const UNMUTE_VIDEO = "muteButton/UNMUTE_VIDEO" as const;

export const muteVideo = () => ({
  type: MUTE_VIDEO,
});
export const unmuteVideo = () => ({
  type: UNMUTE_VIDEO,
});

type MuteButtonAction =
  | ReturnType<typeof muteVideo>
  | ReturnType<typeof unmuteVideo>;
type MuteButtonState = {
  isMuted: boolean | undefined;
};

const initialState: MuteButtonState = {
  isMuted: undefined,
};

function muteButton(
  state: MuteButtonState = initialState,
  action: MuteButtonAction
): MuteButtonState {
  switch (action.type) {
    case MUTE_VIDEO: {
      return (state = { isMuted: true });
    }
    case UNMUTE_VIDEO: {
      return (state = { isMuted: false });
    }
    default:
      return state;
  }
}

export default muteButton;
