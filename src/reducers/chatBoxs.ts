import LocalStorageUtil from "utils/localStorageUtil";

const ADD_CHAT = "chatList/ADD_CHAT" as const;
const ADD_CAHT_LIST = "chatList/ADD_CHAT_LIST" as const;
const TOGGLE_CHAT_LIST = "chatList/TOGGLE_CHAT_LIST" as const;

export const addChat = (chat: Chat) => ({
  type: ADD_CHAT,
  payload: chat,
});

export const addChatList = (chatState: string) => ({
  type: ADD_CAHT_LIST,
  payload: chatState,
});

type ChatAction = ReturnType<typeof addChat> | ReturnType<typeof addChatList>;

export type Chat = {
  id?: number;
  text: string;
  date: string;
  isBot: boolean;
  isAudio: boolean;
};

export type ChatState = {
  isAddByLocalStorage?: boolean;
  currentId: number;
  chatBoxs: Chat[];
};

const initialState: ChatState = {
  isAddByLocalStorage: false,
  currentId: 0,
  chatBoxs: [],
};

function chatBoxs(
  state: ChatState = initialState,
  action: ChatAction
): ChatState {
  switch (action.type) {
    case ADD_CHAT:
      const lastChat: Chat = state.chatBoxs[state.currentId - 1];

      //답변이 들어오기 전에 질문이 들어올 경우 차단
      if (lastChat !== undefined) {
        if (!lastChat.isBot) if (!action.payload.isBot) return state;
      }

      const payload = action.payload;
      const nextId = state.chatBoxs.length + 1;
      let _localStorageUtil: LocalStorageUtil = new LocalStorageUtil();

      state = {
        currentId: nextId,
        chatBoxs: state.chatBoxs.concat({
          id: nextId,
          text: payload.text,
          date: payload.date,
          isBot: payload.isBot,
          isAudio: payload.isAudio,
        }),
      };

      const saveChat: string = JSON.stringify(state);
      _localStorageUtil?.removeLocalStorage();
      _localStorageUtil?.saveToLocalStorage(saveChat);

      _localStorageUtil.checkLocalStorage();
      return {
        ...state,
        isAddByLocalStorage: false,
      };
    case ADD_CAHT_LIST:
      if (action.payload === "") return initialState;
      var obj: ChatState = JSON.parse(action.payload);
      state = obj;
      return {
        ...state,
        isAddByLocalStorage: true,
      };
    default:
      return state;
  }
}

export default chatBoxs;
