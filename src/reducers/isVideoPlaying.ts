const PLAYING = "isVideoPlaying/MUTE_VIDEO" as const;
const NOT_PLAYING = "isVideoPlaying/UNMUTE_VIDEO" as const;

export const videoPlaying = () => ({
  type: PLAYING,
});
export const videoNotPlaying = () => ({
  type: NOT_PLAYING,
});

type ViedoPlayingAction =
  | ReturnType<typeof videoPlaying>
  | ReturnType<typeof videoNotPlaying>;

type VideoPlayingState = {
  isPlaying: boolean;
};

const initialState: VideoPlayingState = {
  isPlaying: false,
};

function isVideoPlaying(
  state: VideoPlayingState = initialState,
  action: ViedoPlayingAction
): VideoPlayingState {
  switch (action.type) {
    case PLAYING: {
      return (state = { isPlaying: true });
    }
    case NOT_PLAYING: {
      return (state = { isPlaying: false });
    }
    default:
      return state;
  }
}

export default isVideoPlaying;
