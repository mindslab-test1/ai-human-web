import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import human from "./human";
import conversation from "./conversation";
import tagWords from "./tagWord";
import chatBoxs from "./chatBoxs";
import saveQuestion from "./saveQuestion";
import muteButton from "./muteButton";
import isVideoPlaying from "./isVideoPlaying";
import pauseButton from "./pauseButton";
import humanIframe from "./humanIframe";
import videoElement from "./videoElement";
import isMindslab from "./isMindslab";

const rootPersistConfig = {
  key: "root",
  storage,
  whitelist: ["human"],
};

const rootReducer = combineReducers({
  human,
  conversation,
  tagWords,
  chatBoxs,
  saveQuestion,
  muteButton,
  isVideoPlaying,
  pauseButton,
  humanIframe,
  videoElement,
  isMindslab,
});

export default persistReducer(rootPersistConfig, rootReducer);
