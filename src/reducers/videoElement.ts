const SET_INIT = "video/SET_INIT" as const;
const SET_INITIAL_VIDEO = "video/SET_INITIAL_VIDEO" as const;
const SET_LOADED_VIDEO = "video/SET_LOADED_VIDEO" as const;
const MUTE_VIDEO = "video/MUTE_VIDEO" as const;
const UNMUTE_VIDEO = "video/UNMUTE_VIDEO" as const;
const PAUSE_VIDEO = "video/SHOW_LIPSYNC" as const;
const PLAY_VIDEO = "video/SHOW_INIT" as const;
const TEMP_PAUSE = "video/TEMP_PAUSE" as const;
const NOT_TEMP_PAUSE = "video/NOT_TEMP_PAUSE" as const;

export const setInit = (videoState: VideoState) => ({
  type: SET_INIT,
  payload: videoState,
});

export const setInitialVideoSrc = (videoSrc: string) => ({
  type: SET_INITIAL_VIDEO,
  payload: videoSrc,
});

export const setLoadedVideoSrc = (videoSrc: string) => ({
  type: SET_LOADED_VIDEO,
  payload: videoSrc,
});

export const muteVideo = () => ({
  type: MUTE_VIDEO,
});

export const unMuteVideo = () => ({
  type: UNMUTE_VIDEO,
});

export const pauseVideo = () => ({
  type: PAUSE_VIDEO,
});

export const playVideo = () => ({
  type: PLAY_VIDEO,
});

export const notTempPause = () => ({
  type: NOT_TEMP_PAUSE,
});

export const tempPause = () => ({
  type: TEMP_PAUSE,
});

type VideoAction =
  | ReturnType<typeof setInit>
  | ReturnType<typeof setInitialVideoSrc>
  | ReturnType<typeof setLoadedVideoSrc>
  | ReturnType<typeof muteVideo>
  | ReturnType<typeof unMuteVideo>
  | ReturnType<typeof pauseVideo>
  | ReturnType<typeof playVideo>
  | ReturnType<typeof notTempPause>
  | ReturnType<typeof tempPause>;

export type VideoState = {
  initialVideo?: HTMLVideoElement;
  loadedVideo?: HTMLVideoElement;
  isMuteVideo?: boolean | undefined;
  isLipsyncPlay?: boolean;
  isPauseVideo?: boolean;
  isTempPauseVideo?: boolean;
};

const initialState: VideoState = {
  initialVideo: undefined,
  loadedVideo: undefined,
  isMuteVideo: undefined,
  isLipsyncPlay: false,
  isPauseVideo: false,
  isTempPauseVideo: false,
};

function videoElemnet(
  state: VideoState = initialState,
  action: VideoAction
): VideoState {
  switch (action.type) {
    case SET_INIT:
      return action.payload;
    case SET_INITIAL_VIDEO:
      if (state.initialVideo !== undefined) {
        const video: HTMLVideoElement = state.initialVideo;
        video.src = action.payload;
        return {
          ...state,
          initialVideo: video,
          isLipsyncPlay: false,
        };
      }
      return state;
    case SET_LOADED_VIDEO:
      if (state.loadedVideo !== undefined) {
        const video: HTMLVideoElement = state.loadedVideo;
        video.src = action.payload;
        return {
          ...state,
          isPauseVideo: false,
          loadedVideo: video,
          isLipsyncPlay: true,
        };
      }
      return state;
    case MUTE_VIDEO:
      if (state.loadedVideo !== undefined) {
        const loadedVideo: HTMLVideoElement = state.loadedVideo;
        loadedVideo.muted = false;
      }
      return {
        ...state,
        isMuteVideo: true,
      };
    case UNMUTE_VIDEO:
      if (state.loadedVideo !== undefined) {
        const loadedVideo: HTMLVideoElement = state.loadedVideo;
        loadedVideo.muted = true;
      }
      return {
        ...state,
        isMuteVideo: false,
      };
    case PAUSE_VIDEO:
      return {
        ...state,
        isPauseVideo: true,
      };
    case PLAY_VIDEO:
      return {
        ...state,
        isPauseVideo: false,
      };
    case TEMP_PAUSE:
      return {
        ...state,
        isTempPauseVideo: true,
      };
    case NOT_TEMP_PAUSE:
      return {
        ...state,
        isTempPauseVideo: false,
      };
    default:
      return state;
  }
}

export default videoElemnet;
