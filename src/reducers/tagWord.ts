const ADD_TAG = "tagWord/ADD_TAG" as const;
const ADD_TAG_LIST = "tagWord/ADD_TAG_LISG" as const;

export const addTagWord = (tag: TagWord) => ({
  type: ADD_TAG,
  payload: tag,
});

export const addTagWordList = (tags: TagWord[]) => ({
  type: ADD_TAG_LIST,
  payload: tags,
});

type TagWordAction =
  | ReturnType<typeof addTagWord>
  | ReturnType<typeof addTagWordList>;

export type TagWord = {
  id: number;
  isTag: boolean;
  path: string;
  text: string;
};
type TagWordState = TagWord[];

const initialState: TagWordState = [];

function tagWords(
  state: TagWordState = initialState,
  action: TagWordAction
): TagWordState {
  switch (action.type) {
    case ADD_TAG:
      return state;
    case ADD_TAG_LIST:
      const payload: TagWordState = action.payload;
      return payload;
    default:
      return state;
  }
}

export default tagWords;
