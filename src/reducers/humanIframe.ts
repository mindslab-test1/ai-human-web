export const subscribeAction = (data: any) => {
  return {
    type: `HUMAN_IFRAME_${data.type}`,
    payload: data,
  };
};

export const sendTextAction = (data: string) => {
  return {
    type: "HUMAN_IFRAME_SEND_TEXT",
    payload: data,
  };
};
export const sendAudioAction = () => {
  return {
    type: "HUMAN_IFRAME_SEND_AUDIO",
    payload: undefined,
  };
};
export const clearLipSync = () => {
  return {
    type: "HUMAN_IFRAME_CLEAR_LIPSYNC",
    payload: null,
  };
};

interface initPorps {
  id?: number;
  loading?: boolean;
  image?: string;
  video?: string;
  lipSyncVideo?: string;
  alert?: {
    type?: string;
    message?: string;
  };
  history: any[];
  debug: any[];
  expectedIntents: any[];
  autoComplete: any[];
  background?: string;
  pose?: string;
  isInitialized: boolean;
}
const initialState: initPorps = {
  id: undefined,
  loading: false,
  image: undefined,
  video: undefined,
  lipSyncVideo: undefined,
  alert: {
    type: undefined,
    message: undefined,
  },
  history: [],
  debug: [],
  expectedIntents: [],
  autoComplete: [],
  background: undefined,
  pose: undefined,
  isInitialized: false,
};
var idNum = 0;
const humanIframeReducer = (state: initPorps = initialState, action: any) => {
  const { type, payload } = action;
  switch (type) {
    case "HUMAN_IFRAME_HELLO":
      return {
        ...state,
        loading: false,
        lipSyncVideo: payload.video,
        alert: {
          type: "HELLO_DONE",
          message: payload.message,
        },
        history: [
          ...state.history,
          {
            type: "BOT",
            message: payload.message,
            date: payload.date,
          },
        ],
        image: payload.image,
        background: payload.background,
        pose: payload.pose,
        expectedIntents: payload.expectedIntents,
      };
    case "HUMAN_IFRAME_SEND_TEXT":
      return {
        ...state,
        loading: true,
        history: [
          ...state.history,
          {
            type: "USER",
            message: payload,
            date: Math.round(new Date().getTime() / 1000),
          },
        ],
        autoComplete: [],
      };
    case "HUMAN_IFRAME_SEND_AUDIO":
      return {
        ...state,
        loading: true,
      };
    case "HUMAN_IFRAME_INIT":
      return state.isInitialized
        ? state
        : {
            ...state,
            loading: false,
            alert: {
              type: "INIT_DONE",
            },
            image: payload.image,
            video: payload.video,
            background: payload.background,
            pose: payload.pose,
            isInitialized: true,
          };
    case "HUMAN_IFRAME_STT":
      if (payload.message === null) {
        return {
          ...state,
          alert: {
            type: "STT_LOADING",
            message: "",
          },
          debug: [...state.debug, payload],
        };
      } else if (payload.message.replaceAll("\n", "") === "") {
        return {
          ...state,
        };
      } else {
        return {
          ...state,
          alert: {
            type: "STT_DONE",
            message: payload.message,
          },
          history: [
            ...state.history,
            {
              type: "USER",
              message: payload.message,
              date: payload.date,
            },
          ],
          debug: [...state.debug, payload],
        };
      }
    case "HUMAN_IFRAME_SDS":
    case "HUMAN_IFRAME_AVATAR":
      return {
        ...state,
        debug: [...state.debug, payload],
      };
    case "HUMAN_IFRAME_ANSWER":
      return {
        ...state,
        id: idNum++,
        loading: false,
        lipSyncVideo: payload.video,
        alert: {
          type: "SDS_DONE",
          message: payload.message,
        },
        history: [
          ...state.history,
          {
            type: "BOT",
            message: payload.message,
            date: payload.date,
          },
        ],
        expectedIntents: payload.expectedIntents,
      };
    case "HUMAN_IFRAME_CLEAR_LIPSYNC":
      return { ...state, lipSyncVideo: undefined };
    case "HUMAN_IFRAME_ERROR":
      return payload.message === "" || payload.message === null
        ? { ...state, loading: false }
        : {
            ...state,
            loading: false,
            lipSyncVideo: payload.video,
            alert: {
              type: "ERROR",
              message: payload.message,
            },
            history: [
              ...state.history,
              {
                type: "BOT",
                message: payload.message,
                date: payload.date,
              },
            ],
          };
    case "HUMAN_IFRAME_AUTOCOMPLETE":
      return {
        ...state,
        autoComplete: payload.autoComplete,
      };
    default:
      return state;
  }
};
export default humanIframeReducer;
