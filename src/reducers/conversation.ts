export const selectItem = (index: Number) => {
  return {
    type: "SELECTED_ITEM",
    payload: index,
  };
};

interface initPorps {
  selectedItem: Number;
}

const initialState: initPorps = {
  selectedItem: 0,
};
function conversationReducer(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case "SELECTED_ITEM":
      return { ...state, selectedItem: payload };
    default:
      return state;
  }

  return state;
}

export default conversationReducer;
