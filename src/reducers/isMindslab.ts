const MINDSLAB = "isMindslab/MINDSLAB" as const;
const NOT_MINDSLAB = "isMindslab/NOT_MINDSLAB" as const;

export const setMindslab = () => ({
  type: MINDSLAB,
});
export const unsetMindslab = () => ({
  type: NOT_MINDSLAB,
});

type IsMindslabAction =
  | ReturnType<typeof setMindslab>
  | ReturnType<typeof unsetMindslab>;
type IsMindslabState = {
  isMindslab: boolean | undefined;
};

const initialState: IsMindslabState = {
  isMindslab: false,
};

function isMindslab(
  state: IsMindslabState = initialState,
  action: IsMindslabAction
): IsMindslabState {
  switch (action.type) {
    case MINDSLAB: {
      return (state = { isMindslab: true });
    }
    case NOT_MINDSLAB: {
      return (state = { isMindslab: false });
    }
    default:
      return state;
  }
}

export default isMindslab;
