import { Chat } from "reducers/chatBoxs";

const ADD_SAVE_QUESTION = "const/SAVE_QUESTION" as const;
const CLOSE_CHAT = "const/CLOSE_CHAT" as const;
const OPEN_CHAT = "const/OPEN_CHAT" as const;
const SEND_SAVED_QUESTION = "const/SEND_QUESTION" as const;

export const addSaveQuestion = (saveChat: Chat) => ({
  type: ADD_SAVE_QUESTION,
  payload: saveChat,
});

export const closeChat = () => ({
  type: CLOSE_CHAT,
});

export const openChat = () => ({
  type: OPEN_CHAT,
});

export const sendSaveQuestion = () => ({
  type: SEND_SAVED_QUESTION,
});

type SaveQuestionAction =
  | ReturnType<typeof addSaveQuestion>
  | ReturnType<typeof closeChat>
  | ReturnType<typeof openChat>
  | ReturnType<typeof sendSaveQuestion>;

type SaveQuestionQState = {
  isopenChat: boolean;
  isSave: boolean;
  savedChat?: Chat;
};

const initialState: SaveQuestionQState = {
  isopenChat: false,
  isSave: false,
  savedChat: {
    id: 0,
    text: "",
    date: "",
    isBot: false,
    isAudio: false,
  },
};

function saveQuestion(
  state: SaveQuestionQState = initialState,
  action: SaveQuestionAction
): SaveQuestionQState {
  switch (action.type) {
    case ADD_SAVE_QUESTION:
      return (state = {
        isopenChat: state.isopenChat,
        isSave: true,
        savedChat: action.payload,
      });
    case CLOSE_CHAT:
      return {
        ...state,
        isopenChat: false,
      };
    case OPEN_CHAT:
      return {
        ...state,
        isopenChat: true,
      };
    case SEND_SAVED_QUESTION:
      return {
        isopenChat: state.isopenChat,
        isSave: false,
        savedChat: {
          id: 0,
          text: "",
          date: "",
          isBot: false,
          isAudio: false,
        },
      };
    default:
      return state;
  }
}

export default saveQuestion;
