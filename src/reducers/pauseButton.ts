const PAUSE_VIDEO = "pauseButton/PAUSE_VIDEO" as const;
const PLAY_VIDEO = "pauseButton/PLAY_VIDEO" as const;
const TEMPOARY_PAUSE_VIDEO = "pauseButton/TEMPOARY_PAUSE_VIDEO" as const;
const NOT_TEMPOARY_PAUSE_VIDEO =
  "pauseButton/NOT_TEMPOARY_PAUSE_VIDEO" as const;

export const pauseVideo = () => ({
  type: PAUSE_VIDEO,
});
export const playVideo = () => ({
  type: PLAY_VIDEO,
});

export const tempPauseVideo = () => ({
  type: TEMPOARY_PAUSE_VIDEO,
});

export const nTempPauseVideo = () => ({
  type: NOT_TEMPOARY_PAUSE_VIDEO,
});

type PauseButtonAction =
  | ReturnType<typeof pauseVideo>
  | ReturnType<typeof playVideo>
  | ReturnType<typeof tempPauseVideo>
  | ReturnType<typeof nTempPauseVideo>;

type PauseButtonState = {
  isPaused: boolean;
  isTempPause: boolean;
};

const initialState: PauseButtonState = {
  isPaused: false,
  isTempPause: false,
};

function muteButton(
  state: PauseButtonState = initialState,
  action: PauseButtonAction
): PauseButtonState {
  switch (action.type) {
    case PAUSE_VIDEO: {
      return (state = { isPaused: true, isTempPause: state.isTempPause });
    }
    case PLAY_VIDEO: {
      return (state = { isPaused: false, isTempPause: state.isTempPause });
    }
    case TEMPOARY_PAUSE_VIDEO: {
      return (state = { isPaused: state.isPaused, isTempPause: true });
    }
    case NOT_TEMPOARY_PAUSE_VIDEO: {
      return (state = { isPaused: state.isPaused, isTempPause: false });
    }
    default:
      return state;
  }
}

export default muteButton;
