import React from "react";
import { Route } from "react-router-dom";
// Redux
import { Provider } from "react-redux";
import { store } from "./store";
import { ThemeProvider } from "styled-components";
import { lightTheme, GlobalStyle } from "styles";
import "styles/css/font.css";

import Display from "./screens/Display";
import DisplayIframe from "./screens/DisplayIframe";

const App = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Provider store={store}>
        <Route path="/" exact>
          <GlobalStyle />
          <Display />
        </Route>
        <Route path="/iframe" exact>
          <DisplayIframe />
        </Route>
      </Provider>
    </ThemeProvider>
  );
};

export default App;
