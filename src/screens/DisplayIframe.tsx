import {
  useEffect,
  useState,
  useCallback,
  Fragment,
  useRef,
  memo,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import queryString from "query-string";
import { Redirect, useLocation } from "react-router-dom";
import { RootState } from "store";
/**
 * stomp object
 */
import { StompIframe } from "utils";
/**
 * elements
 */
import DisplayChatMain from "../components/containers/iframe/displayChatMain";
/**
 * reducers
 */
import { subscribeAction } from "reducers/humanIframe";
import { Chat } from "reducers/chatBoxs";
/**
 * hooks
 */
import { useSetMindslab, useUnsetMindslab } from "hook/useMindslab";
import { useAddChat, useAddChatList } from "hook/useChatBoxs";
import {
  useOpenChat,
  useSaveQuestion,
  useSendSavedQuestion,
  useAddSaveQuestion,
} from "hook/useSaveQuestion";
/**
 * css
 */
import "styles/css/chatbotAvatar.css";
import "styles/css/font.css";
import "styles/css/reset.css";
import "styles/css/all.min.css";
/**
 * localstorage utils
 */
import LocalStorageUtil from "utils/localStorageUtil";

const Display = memo(() => {
  const humanIframe = useSelector((state: RootState) => state.humanIframe);
  /**
   * localstorage object
   */
  const localStorageUtil: LocalStorageUtil = new LocalStorageUtil();
  /**
   * functions to check project ID
   */
  const { search } = useLocation();
  const query = queryString.parse(search);
  const projectId: number = Number(query.projectId);
  /**
   * custom hook objects
   */
  const openChat = useOpenChat();
  const saveQuestion = useSaveQuestion();
  const addSavedQuestion = useAddSaveQuestion();
  const sendSavedQuestion = useSendSavedQuestion();
  const setMindslab = useSetMindslab();
  const unsetMindslab = useUnsetMindslab();
  const dispatch = useDispatch();
  const addChat = useAddChat();
  const addChatList = useAddChatList();
  /**
   * useStates
   */
  const [closedText, setClosedText] = useState("init"); //닫힌 상태에서 사용자에게 보여지는 text 문구
  /**
   * useRefs
   */
  const localChatListRef = useRef("");
  /**
   * on Stomp connected
   */
  StompIframe.client.onConnect = () => {
    StompIframe.client.subscribe("/user/queue/human", (data: any) => {
      dispatch(subscribeAction(JSON.parse(data.body)));
    });
    checkLoaclStorage();
    /**
     * temporarily set mindslab if we use project ID under 0 to not use autocomplete function
     */
    if (projectId < 0) setMindslab();
    else unsetMindslab();

    StompIframe.sendInit(projectId);
  };

  /**
   * useEffects
   */
  useEffect(() => {
    const swiperScript = document.createElement("script");
    swiperScript.src = "/js/swiper.min.js";
    swiperScript.async = false;
    document.body.appendChild(swiperScript);

    const jqueryScript = document.createElement("script");
    jqueryScript.src = "/js/jquery-3.5.1.min.js";
    jqueryScript.async = false;
    document.body.appendChild(jqueryScript);

    const script = document.createElement("script");
    script.src = "/js/chatbotAvatar.js";
    script.async = false;
    document.body.appendChild(script);

    StompIframe.activate();
  }, []);
  /*
   * iframe을 통해 들어오는 데이터를 처리
   */
  useEffect(() => {
    window.addEventListener(
      "message",
      (e) => {
        const message: string = typeof e.data.message;
        if (
          message !== "undefined" &&
          e.data.message !== "openChat" &&
          e.data.message !== "closeChat" &&
          e.data.message !== "fullScreen" &&
          e.data.message !== "mo" &&
          e.data.message !== "pc" &&
          e.data.message !== "requestViewType" &&
          e.data.message !== "moonSquared"
        ) {
          const chat: Chat = {
            text: e.data.message,
            date: "",
            isBot: false,
            isAudio: false,
          };
          if (!saveQuestion.isopenChat) {
            addSavedQuestion(chat);
          } else {
            addChat(chat);
            //   StompIframe.sendText(e.data.message);
          }
        }
      },
      false
    );
  }, []);
  useEffect(() => {
    /**
     * 채팅을 닫은 상태에서 열었을 경우 saveQuestion이 있으면
     * stomp로 데이터 전송
     *  */
    if (saveQuestion.isSave && saveQuestion.isopenChat) {
      addChat(saveQuestion.savedChat);
      sendSavedQuestion();
    }

    /**
     * 닫힌 상태에서 text가 들어올 경우 사용자에게 보여질 text 설정
     */
    if (
      saveQuestion.savedChat?.text !== undefined &&
      saveQuestion.savedChat?.text !== ""
    ) {
      setClosedText(saveQuestion.savedChat?.text);
    }
  }, [saveQuestion]);

  /**
   * useCallbacks
   */
  /**
   * localStorage에 데이터가 있는지 체크 후 넣어줌
   */
  const checkLoaclStorage = useCallback(() => {
    const time: string = new Date().toTimeString().split(" ")[0];
    const hors: number = Number(time.split(":")[0]);
    const min: number = Number(time.split(":")[1]);

    if (localStorageUtil.getLocalStoragetTime() !== null) {
      const storageTime = localStorageUtil.getLocalStoragetTime()!;
      const storageHors = Number(storageTime.split(":")[0]);
      const storageMin = Number(storageTime.split(":")[1]) + 3;

      if (hors > storageHors && min > storageMin) {
        addChatList("");
      }
    }

    if (localStorageUtil.getLocalStorage() !== null) {
      const savedChat: any = localStorageUtil.checkLocalStorage();

      if (localChatListRef.current !== savedChat) {
        addChatList(savedChat);
        localChatListRef.current = savedChat;
      }
    }
  }, []);
  const sendTextToStomp = useCallback((message: string) => {
    StompIframe.sendText(message);
  }, []);

  setInterval(function run() {
    checkLoaclStorage();
  }, 3000);

  if (isNaN(projectId)) {
    return (
      <Redirect
        to={{
          pathname: `/iframe`,
          search: `?projectId=-200`,
        }}
      />
    );
  }

  window.addEventListener("message", (e) => {
    if (e.data.message === "moonSquared") {
      openChat();
      StompIframe.sendInit(projectId);
    }
  });

  return (
    <Fragment>
      <div>
        <button
          type="button"
          id="btn_chatbotAvatar"
          // className={"" + (openChat ? "hide" : "")}
          onClick={() => {
            openChat();
            StompIframe.sendInit(projectId);
          }}
        >
          <span className="txt">
            {closedText === "init" ? (
              projectId !== 2322 ? (
                <p>
                  마음까지 생각하는 마음AI입니다.
                  <br /> 궁금한 점을 물어보거나
                  <br />
                  스크롤을 이동해 보세요
                </p>
              ) : (
                <p>
                  AI 체험에 대해 궁금한 점은, <br />
                  다온에게 물어보세요.
                </p>
              )
            ) : (
              <p>
                <em>{closedText}</em>에 <br />
                대해 소개해 드릴게요.
                <br />
                저를 클릭해 주세요.
              </p>
            )}
          </span>
          <span className="img">
            <img
              src={
                projectId === 2322
                  ? "https://media.maum.ai/media/service-dev/ai-human/display-prod/profile_image/daon.png"
                  : humanIframe.image
              }
              alt="아바타 소라"
            />
          </span>
        </button>

        <DisplayChatMain sendTextToStomp={sendTextToStomp} />
      </div>
    </Fragment>
  );
});

export default Display;
