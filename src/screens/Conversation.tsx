import React, { FC, Fragment, memo } from "react";
import styled, { useTheme } from "styled-components";
import { Column, Row } from "components/containers";
import { RootState } from "store";

import { DebugView, HistoryView } from "components/boxes/conversation";

import { BrowserView, MobileView } from "components/View";
import { Container, StyledButton } from "components/Html";
import { Button2, IconButton } from "components/buttons";
import { ButtonText1 } from "components/texts";

import icoClose from "assets/images/ico_close.svg";
import { useDispatch, useSelector } from "react-redux";
import { selectItem } from "reducers/conversation";

interface ConversationProps {
  isOpen: boolean;
  onClose?: () => void;
}
const ConversationContainer = styled(Container)<ConversationProps>`
  transform: ${(p) => (p.isOpen ? `scale(1)` : `scale(0)`)};
`;
ConversationContainer.defaultProps = {
  position: "absolute",
  boxSizing: "none",
  width: "460px",
  height: "calc(100% - 80px)",
  absoluteTop: "40px",
  absoluteRight: "40px",
  transition: "transform 0.3s ease-out",
  transformOrigin: "450px 10px",
  zIndex: 10,
};

interface ToggleButtonProps {
  items: any[];
  onChange?: (index: number) => void;
}
const ToggleButton: FC<ToggleButtonProps> = memo(({ items, onChange }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const conversation = useSelector((state: RootState) => state.conversation);
  const { selectedItem } = conversation;
  const handleItemButton = (index: number) => (e: any) => {
    e.preventDefault();
    dispatch(selectItem(index));
    if (onChange) onChange(index);
  };

  return (
    <Fragment>
      <BrowserView>
        <Container radius="20px" padding="8px" background={theme.background2}>
          <Row crossAxisAlignment="center">
            {items?.map((v, idx) => (
              <StyledButton key={idx} onClick={handleItemButton(idx)}>
                <Button2
                  width="148px"
                  height="44px"
                  radius="14px"
                  selected={idx === selectedItem}
                  color={theme.palette.transparent}
                  selectedColor={v.color}
                >
                  <ButtonText1
                    color={theme.palette.white}
                    fontSize="12px"
                    verticalAlign="middle"
                  >
                    {v.text}
                  </ButtonText1>
                </Button2>
              </StyledButton>
            ))}
          </Row>
        </Container>
      </BrowserView>
      <MobileView>
        <Container
          width="196px"
          radius="20px"
          padding="6px 8px"
          background={theme.background2}
        >
          <Row crossAxisAlignment="center">
            {items?.map((v, idx) => (
              <StyledButton key={idx} onClick={handleItemButton(idx)}>
                <Button2
                  width="88px"
                  height="28px"
                  radius="14px"
                  selected={idx === selectedItem}
                  color={theme.palette.transparent}
                  selectedColor={v.color}
                >
                  <ButtonText1
                    color={theme.palette.white}
                    fontSize="12px"
                    verticalAlign="middle"
                  >
                    {v.text}
                  </ButtonText1>
                </Button2>
              </StyledButton>
            ))}
          </Row>
        </Container>
      </MobileView>
    </Fragment>
  );
});

interface CloseButtonProps {
  onClose?: () => void;
}
const CloseButton: FC<CloseButtonProps> = memo(({ onClose }) => {
  const theme = useTheme();

  return (
    <StyledButton onClick={onClose}>
      <BrowserView>
        <IconButton
          src={icoClose}
          padding="12px"
          background={theme.background2}
        />
      </BrowserView>
      <MobileView>
        <IconButton
          src={icoClose}
          size="40px"
          padding="0"
          background={theme.background2}
        />
      </MobileView>
    </StyledButton>
  );
});

const Conversation: FC<ConversationProps> = memo(({ isOpen, onClose }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const conversation = useSelector((state: RootState) => state.conversation);
  const { selectedItem: selectedItem } = conversation;
  const items: any[] = [
    { text: "대화 History", color: "#feb951" },
    { text: "AI Process", color: "#0000ff" },
  ];

  return (
    <Fragment>
      <BrowserView>
        <ConversationContainer isOpen={isOpen}>
          <Container
            width="100%"
            height="100%"
            borderWidth="2px"
            borderColor={theme.border1}
            radius="20px"
            padding="16px"
            background={theme.background1}
            backdropFilter="blur(4.9px)"
          >
            <Column crossAxisAlignment="stretch">
              {/* Toggle Button */}
              <Row mainAxisAlignment="spaceBetween" crossAxisAlignment="center">
                <ToggleButton
                  items={items}
                  onChange={(value) => dispatch(selectItem(value))}
                />
                <CloseButton onClose={onClose} />
              </Row>
              {/* Content Body */}
              <Container
                height="calc(100% - 84px)"
                radius="20px"
                marginTop="24px"
                padding="24px 10px 24px 24px"
                background={theme.background5}
              >
                {selectedItem === 0 ? <HistoryView /> : <DebugView />}
              </Container>
            </Column>
          </Container>
        </ConversationContainer>
      </BrowserView>
      <MobileView>
        <ConversationContainer
          isOpen={isOpen}
          width="calc(100% - 40px)"
          height="calc(100% - 40px)"
          absoluteTop="0"
          absoluteRight="0"
          padding="20px"
          radius="20px"
          transformOrigin="100% 0"
        >
          <Container
            width="100%"
            height="100%"
            borderWidth="2px"
            borderColor={theme.border1}
            radius="20px"
            padding="16px"
            background={theme.background1}
            backdropFilter="blur(4.9px)"
          >
            <Column crossAxisAlignment="stretch">
              {/* Toggle Button */}
              <Row mainAxisAlignment="spaceBetween" crossAxisAlignment="center">
                <ToggleButton
                  items={items}
                  onChange={(value) => dispatch(selectItem(value))}
                />
                <CloseButton onClose={onClose} />
              </Row>
              {/* Content Body */}
              <Container
                height="calc(100% - 52px)"
                radius="20px"
                marginTop="12px"
                padding="16px 8px 16px 12px"
                background={theme.background5}
              >
                {selectedItem === 0 ? <HistoryView /> : <DebugView />}
              </Container>
            </Column>
          </Container>
        </ConversationContainer>
      </MobileView>
    </Fragment>
  );
});

export default Conversation;
