import { useState, useEffect, useRef, useCallback, memo } from "react";
import { useLocation, Redirect } from "react-router-dom";
/**
 * components
 */
import { Chromakey } from "../components/Chromakey";
import { Menu, Footer, Recommend, Message } from "components/containers";
import { useTheme } from "styled-components";
import { SizedBox, Column, ScrollContainer } from "components/containers";
import { Modal } from "components";
import {
  Container,
  StyledButton,
  DisplayContainer,
  VideoContainer,
} from "components/Html";
import { Mobile } from "components/View";
import { H1, ButtonText1, BodyText1 } from "components/texts";
import { IconButton, IconTextButton } from "components/buttons";
/**
 * icons
 */
import IcoClose from "assets/images/ico_close.svg";
import IcoCopy from "assets/images/ico_copy.svg";
import LogoAiHumanPC from "assets/images/logo_aiHuman.svg";
import LogoAiHumanMobile from "assets/images/logo_aiHuman_m.svg";
/**
 * redux
 */
import { useSelector, useDispatch } from "react-redux";

import { RootState } from "store";
import { TagWord } from "reducers/tagWord";
import { useAddTagWordList } from "hook/useTagWords";
import { subscribeAction } from "reducers/human";

import { useMediaQuery } from "react-responsive";
import { isIOS } from "react-device-detect";
import queryString from "query-string";
import { Stomp } from "utils";

const Display = memo(() => {
  /**
   * react hooks
   */
  const isMobile = useMediaQuery(Mobile);
  const theme = useTheme();
  const dispatch = useDispatch();
  const addTagWordList = useAddTagWordList();
  const { search } = useLocation();
  const loadedVideoRef = useRef<HTMLVideoElement | null>(null);
  /**
   * useStates
   */
  const [isInited, setIsInited] = useState(false);
  const [modal, setModal] = useState(false);
  const [isMuted, setIsMuted] = useState(false);
  const [isInitMuted, setIsInitMuted] = useState(true);
  const [isInit, setIsInit] = useState(true);
  const [isPaused, setIsPaused] = useState(false);
  /**
   * redux
   */
  const human = useSelector((state: RootState) => state.human);
  const { alert, expectedIntents } = human;

  const query = queryString.parse(search);
  const projectId: number = Number(query.projectId);
  type Tag = {
    id: string;
    text: string;
  };
  const _exportScript = `<script type="text/javascript">window.onload = function () {var iframe = document.createElement("iframe");iframe.id = "myframe";iframe.src = "${window.location.href}";iframe.frameBorder = "0";iframe.allow = "camera;microphone";document.body.appendChild(iframe);document.getElementsByTagName("html")[0].style.height = "100%";document.getElementsByTagName("body")[0].style.height = "100%";window.parent.document.getElementById("myframe").width = "100%";window.parent.document.getElementById("myframe").height = "100%";};</script>`;

  /**
   * start websocket connection
   */
  Stomp.client.onConnect = () => {
    Stomp.client.subscribe("/user/queue/human", (data: any) => {
      dispatch(subscribeAction(JSON.parse(data.body)));
    });
    Stomp.sendInit(projectId);
  };

  useEffect(() => {
    /**
     * mute when mobile iOS due to its policy
     */
    if (isMobile && isIOS) {
      setIsMuted(true);
    }

    /**
     * unmute when user interaction(click) occured due to chrome browser's policy
     */
    window.addEventListener("click", () => {
      if (isInitMuted) setIsInitMuted(false);
    });

    /**
     * activate websocket connection
     */
    Stomp.activate();
  }, []);

  /**
   * if init is done send hello
   */
  useEffect(() => {
    switch (alert?.type) {
      case "INIT_DONE":
        if (!isInited) {
          setIsInited(true);
          Stomp.sendHello();
        }
        return;
      default:
        return;
    }
  }, [alert]);
  /**
   * if recommend questions comes, fill with it
   */
  useEffect(() => {
    if (expectedIntents !== null && expectedIntents !== undefined) {
      const num: number = expectedIntents.length;
      const obj: Tag[] = expectedIntents;
      const tags: TagWord[] = [];

      for (let i = 0; i < num; i++) {
        const tag: TagWord = {
          id: i,
          isTag: true,
          path: "#!",
          text: JSON.stringify(obj[i]).replace('"', "").replace('"', ""),
        };
        tags.push(tag);
      }

      addTagWordList(tags);
    }
  }, [expectedIntents]);
  /**
   * change avatar with project ID
   */
  useEffect(() => {
    if (Stomp.client.connected) Stomp.sendInit(projectId);
    else Stomp.activate();
  }, [projectId]);

  /**
   * pause button's function
   */
  const handlePause = useCallback(() => {
    if (!isInit) {
      if (isPaused) (loadedVideoRef.current as HTMLVideoElement).play();
      else (loadedVideoRef.current as HTMLVideoElement).pause();
      setIsPaused(!isPaused);
    }
  }, [isInit, isPaused]);

  /**
   * set default project ID with -100
   */
  if (isNaN(projectId)) {
    return (
      <Redirect
        to={{
          pathname: `/`,
          search: `?projectId=-100`,
        }}
      />
    );
  }

  return (
    <DisplayContainer>
      <H1 position="relative" zIndex="10" style={{ float: "left" }}>
        <img
          className={isMobile ? "mb" : "pc"}
          style={{ display: "block" }}
          src={isMobile ? LogoAiHumanMobile : LogoAiHumanPC}
          alt="AI Human logo"
        />
      </H1>
      <VideoContainer>
        {(() => {
          return (
            <Chromakey
              isInit={isInit}
              isMuted={isMuted}
              isInitMuted={isInitMuted}
              setIsInit={(value: boolean) => setIsInit(value)}
              loadedVideoRef={loadedVideoRef}
            />
          );
        })()}
      </VideoContainer>
      <Menu
        handleExport={() => {
          setModal(true);
        }}
      />
      <Footer />
      <Message
        isMuted={isMuted}
        isInitMuted={isInitMuted}
        isPaused={isPaused}
        handleMIC={() => {
          if (isInitMuted) return;
          setIsMuted(!isMuted);
        }}
        handlePause={handlePause}
      />
      <Recommend />
      <Modal show={modal}>
        <Column crossAxisAlignment="center">
          <Container
            radius="20px"
            padding="24px 16px"
            backdropFilter="blur(4.9px)"
            background={theme.background1}
          >
            <Column crossAxisAlignment="center">
              <ButtonText1
                lineHeight="1.5"
                fontSize={isMobile ? "14px" : undefined}
                color="#022b49"
              >
                AI Human 내보내기
              </ButtonText1>
              <SizedBox height={isMobile ? "12px" : "24px"} />
              <Container
                radius="20px"
                width={isMobile ? "80vw" : "25vw"}
                height="92px"
                padding="16px"
                background={theme.background2}
              >
                <ScrollContainer>
                  <BodyText1 fontSize="12px">{_exportScript}</BodyText1>
                </ScrollContainer>
              </Container>
              <SizedBox height={isMobile ? "12px" : "24px"} />
              <Container
                radius="20px"
                padding="8px"
                background={theme.background2}
              >
                <StyledButton
                  onClick={() => {
                    navigator.clipboard.writeText(_exportScript);
                  }}
                >
                  <IconTextButton
                    src={IcoCopy}
                    text="내보내기"
                    gap="7px"
                    background="#b86ccd"
                  />
                </StyledButton>
              </Container>
            </Column>
          </Container>
          <SizedBox height={isMobile ? "12px" : "24px"} />
          <StyledButton onClick={() => setModal(false)}>
            <IconButton
              size="36px"
              radius="50%"
              background={theme.background2}
              src={IcoClose}
            />
          </StyledButton>
        </Column>
      </Modal>
    </DisplayContainer>
  );
});

export default Display;
