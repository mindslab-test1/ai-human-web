import React from "react";
import RecordRTC from "recordrtc";
import { StompIframe } from "../utils";
import { Stomp } from "../utils";

let recorder: RecordRTC;
let blob: Blob;

class AudioRecorder {
  async startRecording() {
    try {
      let stream = await navigator.mediaDevices.getUserMedia({
        audio: true,
      });
      recorder = new RecordRTC(stream, {
        type: "audio",
        recorderType: RecordRTC.StereoAudioRecorder,
        numberOfAudioChannels: 1,
        desiredSampRate: 8000,
      });

      recorder.startRecording();
    } catch (err) {
      //console.log(err);
    }
  }

  stopRecording(isFinish?: boolean, isFullscreen?: boolean) {
    try {
      recorder?.stopRecording(() => {
        if (isFinish) {
          blob = recorder.getBlob();
          this.sendAudio(blob, isFullscreen as boolean);
        }
      });
    } catch (err) {
      //console.log(err);
    }
  }

  async sendAudio(blob: Blob, isFullscreen: boolean) {
    let blobAsBytes: any;
    blob.arrayBuffer().then((buffer) => (blobAsBytes = buffer));
    blobAsBytes = await blob.arrayBuffer();
    var reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = function () {
      var base64data = reader.result?.toString();
      var audioBase64Message = base64data
        ?.substr(base64data.indexOf(",") + 1)
        .toString()!;
      if (isFullscreen) Stomp.sendAudio(audioBase64Message);
      else if (!isFullscreen) StompIframe.sendAudio(audioBase64Message);
      //else console.log("audio error");
    };
  }
}

export default AudioRecorder;
