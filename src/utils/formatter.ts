export const time = (timestamp: number): string => {
  const date = new Date(timestamp * 1000);
  const hours = `00${date.getHours()}`.substr(-2);
  const minutes = `00${date.getMinutes()}`.substr(-2);
  const seconds = `00${date.getSeconds()}`.substr(-2);
  return `${hours}:${minutes}:${seconds}`;
};
