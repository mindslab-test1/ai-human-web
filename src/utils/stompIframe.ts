// import SockJS from "sockjs-client";
import { Client } from "@stomp/stompjs";

import { store } from "store";
import { sendTextAction, sendAudioAction } from "reducers/humanIframe";

// const STOMP_SERVER: string = process.env.REACT_APP_STOMP_SERVER || "";
// const sock = new SockJS(STOMP_SERVER);

export const client = new Client({
  brokerURL: process.env.REACT_APP_STOMP_SERVER,
  // webSocketFactory: () => sock,
  heartbeatIncoming: 60000,
  heartbeatOutgoing: 60000,
});
client.debug = (str) => {
  //if (process.env.REACT_APP_MODE === "dev") console.log(str);
};
client.onWebSocketError = (e: any) => {
  //console.log(`WebSocket Error: ${e}`);
};
client.onStompError = (e: any) => {
  //console.log(`Stomp Error: ${e}`);
};
client.onUnhandledReceipt = (e: any) => {
  //console.log(`Unhandled Receipt: ${e}`);
};
client.onUnhandledMessage = (e: any) => {
  //console.log(`Unhandled Message: ${e}`);
};
client.onDisconnect = (e: any) => {
  //console.log(`Websocket disconnected`);
};

export const activate = () => client.activate();

export const sendText = (text: string) => {
  client.publish({
    destination: "/app/floating/textinput",
    body: JSON.stringify({ text: text }),
  });
  store.dispatch(sendTextAction(text));
};

export const sendAudio = (audio: String) => {
  client.publish({
    destination: "/app/floating/audioinput",
    body: JSON.stringify({ audio: audio }),
  });
  store.dispatch(sendAudioAction());
};

export const sendInit = (projectId: number) => {
  client.publish({
    destination: "/app/floating/init",
    body: JSON.stringify({ projectId }),
  });
};

// @Deprecated
// export const keepAlive = () => {
//   client.publish({
//     destination: "/app/floating/keepalive",
//     body: JSON.stringify({}),
//   });
// };

export const sendAutoComplete = (text: string) => {
  client.publish({
    destination: "/app/floating/autocomplete",
    body: JSON.stringify({ text: text }),
  });
};

export const sendHello = () => {
  client.publish({
    destination: "/app/floating/hello",
    body: JSON.stringify({ text: "hello" }),
  });
};
