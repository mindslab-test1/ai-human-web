import { stringify } from "query-string";
import chatBoxs from "reducers/chatBoxs";

class LocalStorageUtil {
  saveToLocalStorage(saveChat: string) {
    const time: string = new Date().toTimeString().split(" ")[0];
    localStorage.setItem("aihuman", saveChat);
    localStorage.setItem("time", time);
  }

  removeLocalStorage() {
    localStorage.clear();
  }

  removeLastDataLocalStorage() {
    let data = JSON.parse(this.getLocalStorage() as string);
    localStorage.removeItem("aihuman");
    type Chat = {
      id?: number;
      text: string;
      date: string;
      isBot: boolean;
      isAudio: boolean;
    };
    type ChatState = {
      currentId: number;
      chatBoxs: Chat[];
    };
    let newdata: ChatState = {
      currentId: 0,
      chatBoxs: [],
    };
    for (let i = 0; i < data["chatBoxs"].length - 1; i++) {
      newdata = {
        currentId: i + 1,
        chatBoxs: newdata.chatBoxs.concat({
          id: data["chatBoxs"][i]["id"],
          text: data["chatBoxs"][i]["text"],
          date: data["chatBoxs"][i]["date"],
          isBot: data["chatBoxs"][i]["isBot"],
          isAudio: data["chatBoxs"][i]["isAudio"],
        }),
      };
    }
    localStorage.setItem("aihuman", JSON.stringify(newdata));
  }

  getLocalStorage() {
    return localStorage.getItem("aihuman");
  }

  getLocalStoragetTime() {
    return localStorage.getItem("time");
  }

  checkLocalStorage() {
    if (localStorage.getItem("aihuman") !== null) {
      const savedChats: any = localStorage.getItem("aihuman");
      return savedChats;
    }
  }
}

export default LocalStorageUtil;
