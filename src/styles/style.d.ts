import "styled-components";

declare module "styled-components" {
  export interface Dimension {
    width?: string;
    height?: string;
  }

  export interface RegularDimension {
    size?: string;
  }

  export interface Font {
    color?: string;
    fontSize?: string;
    fontWeight?: number;
    letterSpacing?: string;
    textAlign?: string;
    lineHeight?: number;
  }

  export interface Device {
    mobileS: number;
    mobileM: number;
    mobileL: number;
    tablet: number;
    laptop: number;
    laptopL: number;
    desktop: number;
  }

  export interface Palette {
    transparent: string;
    white: string;
    white24: string;
    darkBlue: string;
  }

  export interface DefaultTheme {
    palette: Palette;
    background1: string;
    background2: string;
    background3: string;
    background4: string;
    background5: string;
    border1: string;
    text1: string;
    button1: string;
  }
}
