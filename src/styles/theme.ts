import { DefaultTheme, Palette } from "styled-components";

export const palette: Palette = {
  transparent: `transparent`,
  white: `#fff`,
  white24: `rgba(255, 255, 255, 0.24)`,
  darkBlue: `#022B49`,
};

export const lightTheme: DefaultTheme = {
  palette: palette,
  background1: `
    linear-gradient(
      135deg,
      rgba(255, 255, 255, 0.05),
      rgba(255, 255, 255, 0.4)
    );
  `,
  background2: `rgba(0, 0, 0, 0.08)`,
  background3: `rgba(255, 255, 255, 0.5)`,
  background4: `rgba(65, 72, 83, 0.5)`,
  background5: `
    linear-gradient(
      135deg,
      rgba(255, 255, 255, 0.2),
      rgba(255, 255, 255, 0.4)
    );
  `,
  border1: palette.white24,
  text1: palette.darkBlue,
  button1: `#fd4383`,
};
