import React, { FC, memo } from "react";
import styled from "styled-components";
import { absoluteCenter } from "styles";
import { Container } from "components/Html";

const Item = styled.div`
  ${absoluteCenter}
`;

export const Modal: FC<{ show: boolean }> = memo(({ show, children }) => {
  return (
    <Container
      position="fixed"
      absoluteTop="0"
      absoluteLeft="0"
      width="100vw"
      height="100vh"
      display={show ? "flex" : "none"}
    >
      <Item>{children}</Item>
    </Container>
  );
});
