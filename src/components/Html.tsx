import React, { FC } from "react";
import styled from "styled-components";
import { device } from "styles/size";
import { Mobile } from "components/View";
import { useMediaQuery } from "react-responsive";

export interface ContainerProps {
  position?: string;
  absoluteTop?: string;
  absoluteRight?: string;
  absoluteBottom?: string;
  absoluteLeft?: string;
  alignItem?: string;
  display?: string;
  size?: string;
  width?: string;
  maxWidth?: string;
  minWidth?: string;
  height?: string;
  maxHeight?: string;
  minHeight?: string;
  padding?: string;
  paddingTop?: string;
  paddingRight?: string;
  paddingBottom?: string;
  paddingLeft?: string;
  margin?: string;
  marginTop?: string;
  marginRight?: string;
  marginBottom?: string;
  marginLeft?: string;
  border?: string;
  borderWidth?: string;
  borderStyle?: string;
  borderColor?: string;
  boxShadow?: string;
  boxSizing?: string;
  radius?: string;
  background?: string;
  backgroundSize?: string;
  backdropFilter?: string;
  clear?: string;
  float?: string;
  justifyContent?: string;
  textAlign?: string;
  transform?: string;
  transformOrigin?: string;
  transition?: string;
  verticalAlign?: string;
  zIndex?: number;
}
export const Container = styled.div<ContainerProps>`
  align-items: ${(p) => p.alignItem};
  position: ${(p) => p.position};
  top: ${(p) => p.absoluteTop};
  right: ${(p) => p.absoluteRight};
  bottom: ${(p) => p.absoluteBottom};
  left: ${(p) => p.absoluteLeft};
  display: ${(p) => p.display};
  width: ${(p) => (p.size ? p.size : p.width)};
  height: ${(p) => (p.size ? p.size : p.height)};
  max-width: ${(p) => p.maxWidth};
  min-width: ${(p) => p.minWidth};
  max-height: ${(p) => p.maxHeight};
  min-height: ${(p) => p.minHeight};
  padding: ${(p) => p.padding};
  padding-top: ${(p) => p.paddingTop};
  padding-right: ${(p) => p.paddingRight};
  padding-bottom: ${(p) => p.paddingBottom};
  padding-left: ${(p) => p.paddingLeft};
  margin: ${(p) => p.margin};
  margin-top: ${(p) => p.marginTop};
  margin-right: ${(p) => p.marginRight};
  margin-bottom: ${(p) => p.marginBottom};
  margin-left: ${(p) => p.marginLeft};
  border: ${(p) => p.border};
  border-width: ${(p) => p.borderWidth};
  border-style: ${(p) => p.borderStyle};
  border-color: ${(p) => p.borderColor};
  border-radius: ${(p) => p.radius};
  box-shadow: ${(p) => p.boxShadow};
  box-sizing: ${(p) => p.boxSizing};
  background: ${(p) => p.background};
  background-size: ${(p) => p.backgroundSize};
  backdrop-filter: ${(p) => p.backdropFilter};
  -webkit-backdrop-filter: ${(p) => p.backdropFilter};
  clear: ${(p) => p.clear};
  float: ${(p) => p.float};
  justify-content: ${(p) => p.justifyContent};
  text-align: ${(p) => p.textAlign};
  transform: ${(p) => p.transform};
  transform-origin: ${(p) => p.transformOrigin};
  -webkit-transform-origin: ${(p) => p.transformOrigin};
  -ms-transform-origin: ${(p) => p.transformOrigin};
  transition: ${(p) => p.transition};
  vertical-align: ${(p) => p.verticalAlign};
  z-index: ${(p) => p.zIndex};
`;
Container.defaultProps = {
  borderWidth: "0",
  borderStyle: "solid",
  boxSizing: "border-box",
};

export interface StyledULProos {
  gap?: string;
}
export const StyledUL = styled.ul<StyledULProos>`
  > li + li {
    margin-top: ${(p) => p.gap};
  }
`;

export interface StyledListProps {
  position?: string;
  display?: string;
  float?: string;
  clear?: string;
  listStyle?: string;
}
export const StyledList = styled.li<StyledListProps>`
  position: ${(p) => p.position};
  display: ${(p) => p.display};
  float: ${(p) => p.float};
  clear: ${(p) => p.clear};
  list-style: ${(p) => p.listStyle};

  button.question {
    display: inline-block;
    width: 100%;
    height: 100%;
    padding: 10px 12px 10px 32px;
    border: 1px solid rgba(255, 255, 255, 0.24);
    border-radius: 32px;
    box-sizing: border-box;
    background: linear-gradient(
      99deg,
      rgba(255, 255, 255, 0.05),
      rgba(255, 255, 255, 0.4) 100%
    );
    backdrop-filter: blur(4.9px);
    -webkit-backdrop-filter: blur(4.9px);
    font-family: "Noto Sans KR", sans-serif;
    color: #353b67;
    text-align: left;
    font-size: 14px;
    font-weight: 500;
    letter-spacing: -0.63px;
    line-height: 20px;
    transition: border 0.3s ease-out, text-shadow 0.3s ease-out;
    cursor: pointer;
  }

  :hover button.question {
    border: 1px solid #fff;
    text-shadow: 0 0 0 #353b67;
  }

  /*
    * to do not show 대화 history's a tag
    */
  a {
    display: none;
  }

  @media screen and (max-width: 768px) {
    margin: 0 6px 12px;
    padding: 10px 12px 10px 28px;
  }
`;
StyledList.defaultProps = {
  position: "relative",
  listStyle: "none",
  clear: "both",
};

export const StyledButton = styled.button`
  border: none;
  padding: 0;
  margin: 0;
`;

export interface StyledImageProps {
  src?: string;
  alt?: string;
  iconSize?: string;
  iconWidth?: string;
  iconHeight?: string;
}
export const DisplayContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  min-width: 1280px;
  padding: 40px;
  box-sizing: border-box;

  @media ${device.tablet} {
    min-width: 320px;
    padding: 20px;
  }
`;
export const VideoContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  & video {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export const BottomConatiner: FC<{ show: boolean }> = ({ show, children }) => {
  const isMobile = useMediaQuery(Mobile);
  return (
    <Container
      position="absolute"
      absoluteBottom={isMobile ? "20px" : "40px"}
      absoluteLeft="50%"
      transform="translate(-50%, 0)"
      display={show ? undefined : "none"}
    >
      {children}
    </Container>
  );
};
BottomConatiner.defaultProps = {
  show: false,
};
