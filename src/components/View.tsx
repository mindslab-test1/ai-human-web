import React, { FC, memo } from "react";
import { useMediaQuery } from "react-responsive";
import { deviceSize } from "styles/size";

interface Props {
  children: React.ReactElement | null;
}

export const Browser = { minWidth: deviceSize.tablet + 1 };
export const BrowserView: FC<Props> = memo(({ children }) => {
  const browser = useMediaQuery(Browser);
  return browser ? children : null;
});

export const Mobile = { maxWidth: deviceSize.tablet };
export const MobileView: FC<Props> = memo(({ children }) => {
  const mobile = useMediaQuery(Mobile);
  return mobile ? children : null;
});
