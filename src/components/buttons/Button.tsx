import React, { FC, memo } from "react";
import styled from "styled-components";
import { flexCenter } from "styles/mixin";
import { Container, ContainerProps, StyledImageProps } from "components/Html";
import { ButtonText1, ButtonText2 } from "components/texts";
import { Row, Column } from "components/containers";

interface Button1Props {
  radius?: string;
}
export const Button1 = styled(Container)<Button1Props>`
  ${flexCenter}
  border-color: ${(p) => p.borderColor ?? p.theme.palette.white24};
  background: ${(p) => p.background ?? p.theme.background1};
  border-radius: radius;
`;
Button1.defaultProps = {
  backdropFilter: "blur(4.9px)",
  borderWidth: "2px",
  radius: "32px",
};

interface Button2Props {
  selected?: boolean;
  color?: string;
  selectedColor?: string;
}
export const Button2 = styled(Container)<Button2Props>`
  ${flexCenter}
  background: ${(p) => (p.selected ? p.selectedColor : p.color)};
  box-shadow: ${(p) => (p.selected ? p.boxShadow : undefined)};
`;
Button2.defaultProps = {
  color: "gray",
};

interface MainButtonProps extends ContainerProps, StyledImageProps {
  text?: string;
  textGap?: string;
  fontSize?: string;
  fontWeight?: string;
}
export const MainButton: FC<MainButtonProps> = memo(
  ({
    src,
    alt,
    iconSize,
    iconWidth,
    iconHeight,
    text,
    textGap,
    radius,
    fontSize,
    fontWeight,
    ...props
  }) => {
    return (
      <Column crossAxisAlignment="center" inline={true}>
        <Button1 radius={radius} {...props}>
          <img
            src={src}
            alt={alt}
            width={iconSize ?? iconWidth}
            height={iconSize ?? iconHeight}
          />
        </Button1>
        <ButtonText2
          marginTop={textGap}
          textAlign="center"
          fontSize={fontSize}
          fontWeight={fontWeight}
        >
          {text}
        </ButtonText2>
      </Column>
    );
  }
);
MainButton.defaultProps = {
  alt: "",
  textGap: "12px",
};

interface IconButtonProps extends ContainerProps, StyledImageProps {}
export const IconButton: FC<IconButtonProps> = memo(
  ({ src, alt, iconSize, iconWidth, iconHeight, ...props }) => {
    return (
      <Container {...props}>
        <img
          src={src}
          alt={alt}
          width={iconSize ?? iconWidth}
          height={iconSize ?? iconHeight}
        />
      </Container>
    );
  }
);
IconButton.defaultProps = {
  alt: "",
  display: "flex",
  justifyContent: "center",
  alignItem: "center",
  size: "60px",
  radius: "20px",
  background: "gray",
};

const IconTextContainer = styled(Container)`
  &:active {
    opacity: 0.5;
  }
`;

interface IconTextButtonProps extends ContainerProps, StyledImageProps {
  text?: string;
  gap?: string;
}
export const IconTextButton: FC<IconTextButtonProps> = memo(
  ({ src, alt, iconSize, iconWidth, iconHeight, text, gap, ...props }) => {
    return (
      <IconTextContainer {...props}>
        <Row crossAxisAlignment="center" style={{ height: "100%" }}>
          <img
            src={src}
            alt={alt}
            width={iconSize ?? iconWidth}
            height={iconSize ?? iconHeight}
          />
          <ButtonText1 marginLeft={gap} textAlign="center">
            {text}
          </ButtonText1>
        </Row>
      </IconTextContainer>
    );
  }
);
IconTextButton.defaultProps = {
  height: "44px",
  padding: "0 40px",
  radius: "14px",
};
