import React, { FC, memo } from "react";
import styled, { RegularDimension } from "styled-components";
import { device } from "styles/size";

interface ButtonContainerProps extends RegularDimension, ButtonProps {
  boxShadow?: string;
  backdropFilter?: string;
}

export const ButtonContainer = styled.button<ButtonContainerProps>`
  width: 92px;
  height: 92px;
  padding: 6px;
  border-radius: 50%;
  box-sizing: border-box;
  background: rgba(255, 255, 255, 0.05);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);

  & span {
    display: inline-block;
    width: 80px;
    height: 80px;
    border: 1px solid rgba(255, 255, 255, 0.24);
    border-radius: 50%;
    box-shadow: 0 1px 16px 0 rgba(255, 255, 255, 0.2);
    box-sizing: border-box;
    background: rgba(255, 255, 255, 0.2);
    backdrop-filter: blur(5px);
    -webkit-backdrop-filter: blur(5px);
    line-height: 76px;
  }
  & img {
    vertical-align: middle;
  }
  @media ${device.tablet} {
    width: 48px;
    height: 48px;
    padding: 4px;
    & span {
      width: 40px;
      height: 40px;
      line-height: 38px;
    }
    & img {
      width: 24px;
      height: 24px;
    }
  }
`;

interface MicButtonProps extends ButtonProps {
  children?: any;
}

export const MicButton: FC<MicButtonProps> = memo(
  ({ onClick, children, ...props }) => {
    return (
      <ButtonContainer onClick={onClick}>
        <span>{children}</span>
      </ButtonContainer>
    );
  }
);
