import { FC, useRef, useState, useEffect, MutableRefObject, memo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "store";
import { clearLipSync } from "reducers/human";

interface ChromakeyProps {
  isInit: boolean;
  isMuted: boolean;
  isInitMuted: boolean;
  loadedVideoRef: MutableRefObject<HTMLVideoElement | null>;
  setIsInit: (value: boolean) => void;
}

export const Chromakey: FC<ChromakeyProps> = memo(
  ({ isInit, isMuted, isInitMuted, loadedVideoRef, setIsInit }) => {
    /**
     * video refs
     */
    let videoRef = useRef<HTMLVideoElement | null>(null);
    let canvasRef = useRef<HTMLCanvasElement | null>(null);
    const initialVideoRef = useRef<HTMLVideoElement | null>(null);
    const initialCanvasRef = useRef<HTMLCanvasElement | null>(null);
    const loadedCanvasRef = useRef<HTMLCanvasElement | null>(null);
    const backgroundRef = useRef<HTMLDivElement | null>(null);

    const dispatch = useDispatch();

    let video: HTMLVideoElement | null;
    let c2: HTMLCanvasElement | null;
    let ctx2: CanvasRenderingContext2D | null | undefined;
    let width: number | undefined;
    let height: number | undefined;

    const human = useSelector((state: RootState) => state.human);
    /**
     * variable to check which video to be shown
     */
    const [toggleVideo, setToggleVideo] = useState(false);

    /**
     * change video state according to change of video url & background
     */
    useEffect(() => {
      if (human.video !== undefined && human.video !== null) {
        (initialVideoRef.current as HTMLVideoElement).src = human.video;
        initialVideoRef.current?.play();
      }
      return;
    }, [human.video]);
    useEffect(() => {
      if (human.lipSyncVideo !== undefined) {
        setIsInit(false);
        (loadedVideoRef.current as HTMLVideoElement).src = human.lipSyncVideo;
        loadedVideoRef.current?.play();
      }
      return;
    }, [human.lipSyncVideo]);
    useEffect(() => {
      if (human.background !== undefined) {
        (backgroundRef.current as HTMLDivElement).style.backgroundImage =
          "url(" + human.background + ")";
      }
      return;
    }, [human.background]);

    /**
     * start chromakeying function
     */
    const doLoad = () => {
      videoRef = isInit ? initialVideoRef : loadedVideoRef;
      canvasRef = isInit ? initialCanvasRef : loadedCanvasRef;
      video = videoRef.current;
      c2 = canvasRef.current;
      ctx2 = c2?.getContext("2d", {
        alpha: true,
        desynchronized: true,
      });

      ctx2?.clearRect(0, 0, c2?.width as number, c2?.height as number);
      (ctx2 as CanvasRenderingContext2D).imageSmoothingEnabled = true;
      (ctx2 as CanvasRenderingContext2D).strokeStyle = "hsl(0,0%,50%)";
      (ctx2 as CanvasRenderingContext2D).setTransform(1, 0, 0, 1, 0.5, 0.5);
      width = c2?.width;
      height = c2?.height;
      processor.timerCallback();
    };

    const processor = {
      timerCallback: function () {
        if (video?.paused || video?.ended) {
          return;
        }
        if (!document.hidden) {
          this.computeFrame();
        }
        let self = this;
        setTimeout(function () {
          self.timerCallback();
        }, 50);
      },

      computeFrame: function () {
        ctx2?.drawImage(
          video as HTMLVideoElement,
          0,
          0,
          width as number,
          height as number
        );

        let frame: ImageData | undefined = ctx2?.getImageData(
          0,
          0,
          width as number,
          height as number
        );
        for (
          let i = 0;
          i <= Math.floor((frame as ImageData).data.length);
          i += 4
        ) {
          let r = (frame as ImageData).data[i];
          let g = (frame as ImageData).data[i + 1];
          let b = (frame as ImageData).data[i + 2];
          if (
            (g > 80 && r < 60 && b < 60) ||
            (g > 110 && r < 90 && b < 90) ||
            (g > 130 && r < 110 && b < 110) ||
            (g > 170 && r < 150 && b < 150) ||
            (g > 190 && r < 170 && b < 170) ||
            (g > 210 && r < 200 && b < 200)
          ) {
            (frame as ImageData).data[i + 3] = 0;
          }
        }
        ctx2?.putImageData(frame as ImageData, 0, 0);
        return;
      },
    };
    return (
      <div
        className="video"
        ref={backgroundRef}
        style={{
          overflow: "hidden",
          position: "absolute",
          right: 0,
          bottom: 0,
          width: "100%",
          height: "100%",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      >
        <video
          style={{
            opacity: 0,
            outline: "none",
          }}
          src={human.video}
          autoPlay
          playsInline
          muted
          loop
          crossOrigin="anonymous"
          ref={initialVideoRef}
          onPlaying={() => {
            if (!toggleVideo) {
              setToggleVideo(!toggleVideo);
            }
            doLoad();
          }}
          onLoadedMetadata={(e) => {
            (initialCanvasRef.current as HTMLCanvasElement).width =
              e.currentTarget.videoWidth;
            (initialCanvasRef.current as HTMLCanvasElement).height =
              e.currentTarget.videoHeight;
          }}
        ></video>
        <canvas
          className="video_vid"
          ref={initialCanvasRef}
          style={
            human.pose === "whole"
              ? {
                  display: toggleVideo ? "block" : "none",
                  position: "absolute",
                  bottom: 0,
                  left: "50%",
                  height: "100%",
                  color: "#ddd",
                  transform: "translate(-52%,30%)",
                  zIndex: 0,
                }
              : {
                  display: toggleVideo ? "block" : "none",
                  position: "absolute",
                  top: 0,
                  left: "50%",
                  height: "100%",
                  color: "#ddd",
                  transform: "translate(-48.5%,0)",
                  zIndex: 0,
                }
          }
        ></canvas>
        <video
          style={{
            opacity: 0,
            outline: "none",
          }}
          playsInline
          crossOrigin="anonymous"
          src={human.lipSyncVideo}
          muted={isMuted || isInitMuted}
          ref={loadedVideoRef}
          onPlaying={() => {
            if (toggleVideo) {
              setToggleVideo(!toggleVideo);
            }
            (initialVideoRef.current as HTMLVideoElement).pause();
            doLoad();
          }}
          onLoadedMetadata={(e) => {
            (loadedCanvasRef.current as HTMLCanvasElement).width =
              e.currentTarget.videoWidth;
            (loadedCanvasRef.current as HTMLCanvasElement).height =
              e.currentTarget.videoHeight;
          }}
          onEnded={() => {
            setIsInit(true);
            dispatch(clearLipSync());
            (initialVideoRef.current as HTMLVideoElement).play();
          }}
        ></video>
        <canvas
          className="video_vid"
          ref={loadedCanvasRef}
          style={
            human.pose === "whole"
              ? {
                  display: toggleVideo ? "none" : "block",
                  position: "absolute",
                  bottom: 0,
                  left: "50%",
                  height: "100%",
                  color: "#ddd",
                  transform: "translate(-52%,30%)",
                  zIndex: 0,
                }
              : {
                  display: toggleVideo ? "none" : "block",
                  position: "absolute",
                  top: 0,
                  left: "50%",
                  height: "100%",
                  color: "#ddd",
                  transform: "translate(-48.5%,0)",
                  zIndex: 0,
                }
          }
        ></canvas>
      </div>
    );
  }
);
