import React, { Fragment, FC, memo } from "react";
import { useTheme } from "styled-components";
/**
 * components
 */
import { Container, StyledButton } from "components/Html";
import { BrowserView, MobileView } from "components/View";
import { ButtonText1, MessageText } from "components/texts";
import { IconButton } from "components/buttons";
/**
 * icons
 */
import IcoSpeaker from "assets/images/ico_speaker.svg";
import IcoMute from "assets/images/ico_mute.svg";
import IcoPlay from "assets/images/ico_play.svg";
import IcoPause from "assets/images/ico_pause.svg";
/**
 * redux
 */
import { useSelector } from "react-redux";
import { RootState } from "store";
import dompurify from "dompurify";

interface MessageProps {
  isMuted: boolean;
  isInitMuted: boolean;
  isPaused: boolean;
  handlePause: () => void;
  handleMIC: () => void;
}

export const Message: FC<MessageProps> = memo(
  ({ isMuted, isInitMuted, isPaused, handlePause, handleMIC }) => {
    const human = useSelector((state: RootState) => state.human);
    const { alert } = human;
    const theme = useTheme();

    return (
      <Fragment>
        <BrowserView>
          <Fragment>
            <Container
              display={alert?.message === undefined ? "none" : "block"}
              position="absolute"
              absoluteTop="150px"
              absoluteLeft="50%"
              maxWidth="280px"
              paddingTop="16px"
              paddingBottom="16px"
              paddingLeft="14px"
              paddingRight="14px"
              radius="20px"
              boxShadow="0 1px 16px 0 rgba(255, 255, 255, 0.2)"
              boxSizing="border-box"
              background="linear-gradient(110deg, rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.4))"
              backdropFilter="blur(4.9px)"
              transform="translate(-50%, 0)"
              textAlign="center"
            >
              {(() => {
                switch (alert?.type) {
                  case "STT_LOADING":
                    return (
                      <Container
                        radius="20px"
                        padding="0 12px"
                        background={theme.background2}
                      >
                        <ButtonText1 lineHeight="1.5">
                          음성을 텍스트로 변환중입니다.
                        </ButtonText1>
                      </Container>
                    );
                  case "SDS_DONE":
                  case "HELLO_DONE":
                  case "ERROR":
                    return (
                      <MessageText
                        maxHeight="100px"
                        fontSize="16px"
                        lineHeight="25px"
                        dangerouslySetInnerHTML={{
                          __html: dompurify.sanitize(alert?.message),
                        }}
                      />
                    );

                  default:
                    return;
                }
              })()}
              <Container
                display="flex"
                position="absolute"
                justifyContent="center"
                alignItem="center"
                absoluteTop="-65px"
                absoluteLeft="50%"
                transform="translate(-50%, 0)"
              >
                <StyledButton onClick={handleMIC}>
                  <IconButton
                    background="rgba(0, 0, 0, 0.12)"
                    src={isMuted || isInitMuted ? IcoMute : IcoSpeaker}
                    size="48px"
                    radius="50%"
                    iconSize="24px"
                    margin="0 8px"
                  />
                </StyledButton>
                <StyledButton onClick={handlePause}>
                  <IconButton
                    background="rgba(0, 0, 0, 0.12)"
                    src={isPaused ? IcoPlay : IcoPause}
                    size="48px"
                    radius="50%"
                    iconSize="24px"
                    margin="0 8px"
                  />
                </StyledButton>
              </Container>
            </Container>
          </Fragment>
        </BrowserView>
        <MobileView>
          <Container
            position="relative"
            margin="65px auto 0"
            transform="none"
            absoluteTop="0"
            absoluteLeft="0"
            maxWidth="280px"
            paddingTop="16px"
            paddingBottom="16px"
            paddingLeft="14px"
            paddingRight="14px"
            radius="20px"
            boxShadow="0 1px 16px 0 rgba(255, 255, 255, 0.2)"
            boxSizing="border-box"
            background="linear-gradient(110deg, rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.4))"
            backdropFilter="blur(4.9px)"
            textAlign="center"
          >
            {(() => {
              switch (alert?.type) {
                case "STT_LOADING":
                  return (
                    <Container
                      radius="20px"
                      padding="0 12px"
                      background={theme.background2}
                    >
                      <ButtonText1 lineHeight="1.5">
                        음성을 텍스트로 변환중입니다.
                      </ButtonText1>
                    </Container>
                  );
                case "SDS_DONE":
                case "HELLO_DONE":
                case "ERROR":
                  return (
                    <MessageText
                      maxWidth="100%"
                      maxHeight="50px"
                      fontSize="12px"
                      lineHeight="18px"
                      dangerouslySetInnerHTML={{
                        __html: dompurify.sanitize(alert?.message),
                      }}
                    />
                  );

                default:
                  return;
              }
            })()}
            <Container
              display="flex"
              position="absolute"
              justifyContent="center"
              alignItem="center"
              absoluteTop="auto"
              absoluteBottom="-46px"
              absoluteRight="auto"
              absoluteLeft="50%"
              transform="translate(-50%, 0)"
            >
              <StyledButton onClick={handleMIC}>
                <IconButton
                  background="rgba(0, 0, 0, 0.12)"
                  src={isMuted || isInitMuted ? IcoMute : IcoSpeaker}
                  size="36px"
                  radius="50%"
                  iconSize="20px"
                  margin="0 8px"
                />
              </StyledButton>
              <StyledButton onClick={handlePause}>
                <IconButton
                  background="rgba(0, 0, 0, 0.12)"
                  src={isPaused ? IcoPlay : IcoPause}
                  size="36px"
                  radius="50%"
                  iconSize="20px"
                  margin="0 8px"
                />
              </StyledButton>
            </Container>
          </Container>
        </MobileView>
      </Fragment>
    );
  }
);
