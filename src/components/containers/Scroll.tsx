import styled from "styled-components";

export const ScrollContainer = styled.div`
  ::-webkit-scrollbar {
    width: 4px;
    height: 4px;
    border-radius: 0;
  } /* 스크롤바의 width */
  ::-webkit-scrollbar-track {
    background: transparent;
  } /* 스크롤바의 전체 배경색 */
  ::-webkit-scrollbar-thumb {
    border-radius: 2px;
    background: #fff;
  } /* 스크롤바 색 */
  ::-webkit-scrollbar-button {
    display: none;
  } /* 위 아래 버튼 (버튼 없애기를 함) */
  overflow-y: auto;
  width: 100%;
  height: 100%;
  padding: 0 12px 0 0;
  box-sizing: border-box;
  background: transparent;
  overflow-wrap: break-word;
`;
