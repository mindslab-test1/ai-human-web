import { useState, Fragment, FC, memo } from "react";
import { useMediaQuery } from "react-responsive";
/**
 * components
 */
import { SendText, SendAudio } from "components/boxes/conversation";
import { SizedBox, Column } from "components/containers";
import { Container, StyledButton } from "components/Html";
import { BrowserView, MobileView, Mobile } from "components/View";
import { ButtonText1 } from "components/texts";
import { MainButton } from "components/buttons";
/**
 * icons
 */
import IcoMic from "assets/images/ico_mic.svg";
import IcoText from "assets/images/ico_text.svg";
/**
 * redux
 */
import { RootState } from "store";
import { useSelector } from "react-redux";

interface FooterProps {
  show: boolean;
}

const BottomConatiner: FC<FooterProps> = memo(({ show, children }) => {
  const isMobile = useMediaQuery(Mobile);

  return (
    <Container
      position="absolute"
      absoluteBottom={isMobile ? "20px" : "40px"}
      absoluteLeft="50%"
      transform="translate(-50%, 0)"
      display={show ? undefined : "none"}
    >
      {children}
    </Container>
  );
});
BottomConatiner.defaultProps = {
  show: false,
};
export const Footer = memo(() => {
  const [showSendAudio, setShowSendAudio] = useState(false);
  const [showSendText, setShowSendText] = useState(false);

  const human = useSelector((state: RootState) => state.human);
  const { alert } = human;

  const isMobile = useMediaQuery(Mobile);

  return (
    <Fragment>
      <BottomConatiner show={showSendText || showSendAudio ? false : true}>
        <BrowserView>
          <Fragment>
            <StyledButton onClick={() => setShowSendAudio(true)}>
              <MainButton
                size="84px"
                src={IcoMic}
                text="음성 대화"
                textGap="6px"
              />
            </StyledButton>
            <SizedBox width="36px" />
            <StyledButton onClick={() => setShowSendText(true)}>
              <MainButton
                size="84px"
                src={IcoText}
                text="텍스트 대화"
                textGap="6px"
              />
            </StyledButton>
          </Fragment>
        </BrowserView>
        <MobileView>
          <Fragment>
            <StyledButton onClick={() => setShowSendAudio(true)}>
              <MainButton
                src={IcoMic}
                size="48px"
                radius="20px"
                iconSize="24px"
                text="음성 대화"
                textGap="4px"
                fontSize="10px"
                fontWeight="400"
              />
            </StyledButton>
            <SizedBox width="20px" />
            <StyledButton onClick={() => setShowSendText(true)}>
              <MainButton
                src={IcoText}
                size="48px"
                radius="20px"
                iconSize="24px"
                text="텍스트 대화"
                textGap="4px"
                fontSize="10px"
                fontWeight="400"
              />
            </StyledButton>
          </Fragment>
        </MobileView>
      </BottomConatiner>
      {/* Text Input */}
      <BottomConatiner show={showSendText}>
        <SendText onClose={() => setShowSendText(false)} />
      </BottomConatiner>
      {/* Audio Input */}
      <BottomConatiner show={showSendAudio}>
        <Column crossAxisAlignment="center">
          {alert?.type === "STT_DONE" && alert.message !== "" ? (
            <Container
              background="rgba(0, 0, 0, 0.16)"
              radius="20px"
              padding="16px"
            >
              <ButtonText1 lineHeight="1.5" letterSpacing="0">
                {alert.message}
              </ButtonText1>
            </Container>
          ) : null}
          <SizedBox height={isMobile ? "12px" : "24px"} />
          <SendAudio onClose={() => setShowSendAudio(false)} />
        </Column>
      </BottomConatiner>
    </Fragment>
  );
});
