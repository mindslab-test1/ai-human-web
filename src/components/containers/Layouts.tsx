import React from "react";
import styled, { Dimension } from "styled-components";

interface Float {
  right?: boolean;
}
export const Float = styled.div<Float>`
  float: ${(p) => (p.right ? "right" : "left")};
`;
Float.defaultProps = {
  right: false,
};

export const SizedBox = styled.div<Dimension>`
  display: inline-block;
  width: ${(p) => p.width};
  height: ${(p) => p.height};
`;

interface FlexDirection {
  inline?: boolean;
  reverse?: boolean;
  mainAxisAlignment?: string;
  crossAxisAlignment?: string;
  padding?: string;
  margin?: string;
}
export const Column = styled.div<FlexDirection>`
  padding: ${(p) => p.padding};
  margin: ${(p) => p.margin};
  display: ${(p) => (p.inline ? "inline-flex" : "flex")};
  height: 100%;
  flex-direction: ${(p) => (p.reverse ? "column-reverse" : "column")};
  justify-content: ${(p) => {
    switch (p.mainAxisAlignment) {
      case "start":
        return "flex-start";
      case "end":
        return "flex-end";
      case "center":
        return "center";
      case "spaceBetween":
        return "space-between";
      case "spaceAround":
        return "space-around";
      default:
        return undefined;
    }
  }};
  align-items: ${(p) => {
    switch (p.crossAxisAlignment) {
      case "stretch":
        return "stretch";
      case "start":
        return "flex-start";
      case "end":
        return "flex-end";
      case "center":
        return "center";
      case "spaceBetween":
        return "space-between";
      case "spaceAround":
        return "space-around";
      default:
        return undefined;
    }
  }};
`;
Column.defaultProps = {
  inline: false,
  reverse: false,
};
export const Row = styled.div<FlexDirection>`
  padding: ${(p) => p.padding};
  margin: ${(p) => p.margin};
  display: ${(p) => (p.inline ? "inline-flex" : "flex")};
  flex-direction: ${(p) => (p.reverse ? "row-reverse" : "row")};
  justify-content: ${(p) => {
    switch (p.mainAxisAlignment) {
      case "start":
        return "flex-start";
      case "end":
        return "flex-end";
      case "center":
        return "center";
      case "spaceBetween":
        return "space-between";
      case "spaceAround":
        return "space-around";
      default:
        return undefined;
    }
  }};
  align-items: ${(p) => {
    switch (p.crossAxisAlignment) {
      case "stretch":
        return "stretch";
      case "start":
        return "flex-start";
      case "end":
        return "flex-end";
      case "center":
        return "center";
      case "spaceBetween":
        return "space-between";
      case "spaceAround":
        return "space-around";
      default:
        return undefined;
    }
  }};
`;
Row.defaultProps = {
  inline: false,
  reverse: false,
};
interface FlexItemProps {
  flex?: string;
  grow?: number;
  shrink?: number;
  basis?: string;
  align?: string;
}
export const FlexItem = styled.span<FlexItemProps>`
  flex: ${(p) => p.flex};
  flex-grow: ${(p) => p.grow};
  flex-shrink: ${(p) => p.shrink};
  flex-basis: ${(p) => p.basis};
  align-self: ${(p) => p.align};
`;

interface StyledInputProps extends Dimension {}
export const StyledInput = styled.input<StyledInputProps>`
  width: ${(p) => p.width};
  height: ${(p) => p.height};
  padding: 0 12px;
  border-radius: 14px;
  box-shadow: 0 6px 12px 0 rgb(0 0 0 / 10%);
  box-sizing: border-box;
  color: #414853;
  font-size: 14px;
  font-weight: 700;
  letter-spacing: -0.63px;
  line-height: 45px;
  ::placeholder {
    color: #c9d0d8;
    font-size: 14px;
    font-weight: 400;
    letter-spacing: -0.63px;
  }
`;
StyledInput.defaultProps = {};
