import React, { FC, memo } from "react";
import { ContainerProps, Container } from "components";

interface ProfileProps extends ContainerProps {
  src?: string;
}
export const Profile: FC<ProfileProps> = memo(({ src, ...props }) => {
  return (
    <Container
      {...props}
      background={`url("${src}") 50% 50% no-repeat`}
    ></Container>
  );
});
Profile.defaultProps = { radius: "50%", backgroundSize: "cover" };
