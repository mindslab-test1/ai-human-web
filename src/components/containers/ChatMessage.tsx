import React, { FC, memo } from "react";
import { ContainerProps, Container } from "components/Html";

interface ChatMessageProps extends ContainerProps {
  right?: boolean;
}
export const ChatMessage: FC<ChatMessageProps> = memo(
  ({ right, radius, children, ...props }) => {
    return (
      <Container
        {...props}
        radius={
          right
            ? `${radius} ${radius} 0 ${radius}`
            : `${radius} ${radius} ${radius} 0`
        }
      >
        {children}
      </Container>
    );
  }
);
ChatMessage.defaultProps = {
  right: false,
  radius: "20px",
};
