import React, { useState, Fragment, FC, memo } from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
/**
 * components
 */
import { SizedBox } from "components/containers";
import Conversation from "screens/Conversation";
import { Container, StyledButton } from "components/Html";
import { BrowserView, MobileView } from "components/View";
import { MainButton } from "components/buttons";
/**
 * icons
 */
import IcoTalk from "assets/images/ico_talk.svg";
import IcoSiren from "assets/images/ico_siren.svg";
import IcoExport from "assets/images/ico_export.svg";

interface MenuProps {
  handleExport: () => void;
}

export const Menu: FC<MenuProps> = memo(({ handleExport }) => {
  const human = useSelector((state: RootState) => state.human);
  const [isTalkActive, setIsTalkActive] = useState(false);
  const handleTalk = () => setIsTalkActive(!isTalkActive);
  const handleReport = () =>
    window.open(
      "https://docs.google.com/spreadsheets/d/1ow7xAQXB074dOtJipjZeckypDqboKpEe5_Jf7xDjRgE/edit#gid=1016439381",
      "_blank"
    );
  return (
    <Fragment>
      <Container
        position="relative"
        float="right"
        style={{
          visibility: isTalkActive ? "hidden" : "visible",
        }}
      >
        <BrowserView>
          <Fragment>
            {human.expectedIntents === null ||
            human.expectedIntents.length === 0 ? null : (
              <StyledButton onClick={handleReport} className="styledButton">
                <MainButton
                  size="64px"
                  src={IcoSiren}
                  text="오류/개선 신고"
                  textGap="6px"
                  radius="24px"
                />
              </StyledButton>
            )}

            <SizedBox width="36px" />
            <StyledButton onClick={handleExport}>
              <MainButton
                size="64px"
                src={IcoExport}
                text="내보내기"
                textGap="6px"
                radius="24px"
              />
            </StyledButton>
            <SizedBox width="36px" />
            <StyledButton
              onClick={handleTalk}
              style={{
                visibility: isTalkActive ? "hidden" : "visible",
              }}
            >
              <MainButton
                size="64px"
                src={IcoTalk}
                text="대화 보기"
                textGap="6px"
                radius="24px"
              />
            </StyledButton>
          </Fragment>
        </BrowserView>
        <MobileView>
          <Fragment>
            <StyledButton
              onClick={handleReport}
              style={{
                display:
                  human.expectedIntents === null ||
                  human.expectedIntents.length === 0
                    ? "none"
                    : "block",
              }}
            >
              <MainButton
                src={IcoSiren}
                size="40px"
                radius="16px"
                iconSize="20px"
                text="오류/개선 신고"
                textGap="4px"
                fontSize="10px"
                fontWeight="400"
              />
            </StyledButton>
            <SizedBox width="8px" />
            <StyledButton onClick={handleExport}>
              <MainButton
                src={IcoExport}
                size="40px"
                radius="16px"
                iconSize="20px"
                text="내보내기"
                textGap="4px"
                fontSize="10px"
                fontWeight="400"
              />
            </StyledButton>
            <SizedBox width="16px" />
            <StyledButton
              onClick={handleTalk}
              style={{
                visibility: isTalkActive ? "hidden" : "visible",
              }}
            >
              <MainButton
                src={IcoTalk}
                size="40px"
                radius="16px"
                iconSize="20px"
                text="대화 보기"
                textGap="4px"
                fontSize="10px"
                fontWeight="400"
              />
            </StyledButton>
          </Fragment>
        </MobileView>
      </Container>
      <Conversation isOpen={isTalkActive} onClose={handleTalk} />
    </Fragment>
  );
});
