import { FC, memo } from "react";
/**
 * custom hooks
 */
import { useCloseChat } from "hook/useSaveQuestion";
import queryString from "query-string";
import { useLocation } from "react-router-dom";

/**
 * ChatHeaderPage's props
 */
type ChatHeaderProps = {
  pcOpenHandler: () => void;
};

const ChatHeaderPage: FC<ChatHeaderProps> = memo(({ pcOpenHandler }) => {
  /**
   * custom hook objects
   */
  const closeChat = useCloseChat();
  const { search } = useLocation();
  const query = queryString.parse(search);
  const projectId: number = Number(query.projectId);
  return (
    <div className="cont_top">
      {projectId===2322?<h1 className="logo_moon" />:<h1 className="logo" />}
      <ul className="cont_ul">
        <li>
          <button
            type="button"
            className="btn_setting"
            onClick={() =>
              window.open("https://builder.maum.ai/landing", "_blank")
            }
          >
            <i className="fa fa-cog" />
            설정
          </button>
        </li>
        <li>
          <button type="button" className="btn_full" onClick={pcOpenHandler}>
            <i className="fa fa-window-maximize" />
            전체화면
          </button>
        </li>
        <li>
          <button type="button" className="btn_minimize" onClick={closeChat}>
            <i className="fa fa-window-close" />
            닫기
          </button>
        </li>
      </ul>
    </div>
  );
});

export default ChatHeaderPage;
