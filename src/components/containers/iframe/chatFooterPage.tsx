import { useState, FC, useRef, memo } from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
import AudioRecorder from "utils/AudioRecorder";
import "styles/css/swiper.min.css";
/**
 * components
 */
import { SwiperBox, VoiceBox, TextAreaBox } from "components/boxes/iframe";
/**
 * props
 */
type FooterProps = {};

const ChatFooterPage: FC<FooterProps> = memo(() => {
  /**
   * audio object
   */
  const _audioRecorder: AudioRecorder = new AudioRecorder();
  /**
   * useState variables
   */
  const [isQuick, setIsQuick] = useState(true);
  const [isKeyboard, setIsKeyboard] = useState(false);
  const [isMIC, setIsMIC] = useState(false);
  const [isTyped, setIsTyped] = useState(false);
  /**
   * useRef variable
   */
  const textRef = useRef<HTMLInputElement | null>(null);

  /**
   * useSelector
   */
  /**
   * get loading from humanIframe which means is loading avatar
   */
  const { loading } = useSelector(
    (state: RootState) => ({
      loading: state.humanIframe.loading,
    }),
    (prev, next) => {
      return prev.loading === next.loading;
    }
  );

  /**
   * start or stop recording
   * @param isRecord : is voice recording
   * @param isFinish : does user wants to finish recording
   */
  const audioHandler = (isRecord: boolean, isFinish?: boolean) => {
    if (!loading) {
      if (isRecord) {
        _audioRecorder.startRecording();
      } else if (!isRecord) {
        _audioRecorder?.stopRecording(isFinish, false);
      } else {
        //console.log("error");
      }
    }
  };

  /**
   * onclick function of footer bar
   * icon consist with just icon image and border icon so it have to detect both of them
   * switch's ternary operator means detecting both of them
   * @param e : button click event
   */
  const bottomControllerClick = (e: any) => {
    let buttonIcon = e.target.className;
    let button = e.target.parentElement.className;
    switch (button === "btm_btn_inner" ? buttonIcon : button) {
      case "controller_btn btn_button_cvsn":
        setIsQuick((prevIsQuick) => true);
        setIsKeyboard((prevIsKeyboard) => false);
        setIsMIC((prevIsMIC) => false);
        audioHandler(false, false);
        return;
      case "controller_btn btn_text_cvsn":
        setIsQuick((prevIsQuick) => false);
        setIsKeyboard((prevIsKeyboard) => true);
        setIsMIC((prevIsMIC) => false);
        audioHandler(false, false);
        (textRef.current as HTMLInputElement).focus();
        return;
      case "controller_btn btn_voice_cvsn":
        setIsQuick((prevIsQuick) => true);
        setIsKeyboard((prevIsKeyboard) => false);
        setIsMIC((prevIsMIC) => true);
        audioHandler(true, false);
        return;
      case "controller_btn btn_button_cvsn active":
        setIsQuick((prevIsQuick) => true);
        setIsKeyboard((prevIsKeyboard) => false);
        setIsMIC((prevIsMIC) => false);
        audioHandler(false, false);
        return;
      case "controller_btn btn_text_cvsn active":
        setIsQuick((prevIsQuick) => false);
        setIsKeyboard((prevIsKeyboard) => true);
        setIsMIC((prevIsMIC) => false);
        audioHandler(false, false);
        (textRef.current as HTMLInputElement).focus();
        return;
      case "controller_btn btn_voice_cvsn active":
        setIsQuick((prevIsQuick) => false);
        setIsKeyboard((prevIsKeyboard) => false);
        setIsMIC((prevIsMIC) => true);
        audioHandler(true, false);
        return;
      default:
        return;
    }
  };

  return (
    <div className="cont_btm">
      {/* footer bar's content domain*/}
      <div
        className={"conversationRecogBox button" + (isQuick ? " active" : "")}
      >
        <SwiperBox />
      </div>
      <div
        className={
          "conversationRecogBox text" +
          (isKeyboard ? (isTyped ? " active on" : " active") : "")
        }
      >
        <TextAreaBox
          setIsTyped={setIsTyped}
          textRef={textRef}
          loading={loading}
        />
      </div>

      <div className={"conversationRecogBox voice" + (isMIC ? " active" : "")}>
        <VoiceBox audioHandler={audioHandler} />
      </div>
      {/* footer bar */}
      <div className="btm_btn_area">
        <div className="btm_btn_inner">
          <button
            type="button"
            className={
              "controller_btn btn_button_cvsn" + (isQuick ? " active" : "")
            }
            data-btn="button"
            onClick={bottomControllerClick}
          >
            <i className="fa fa-bolt"></i>버튼대화
          </button>
          <button
            type="button"
            className={
              "controller_btn btn_text_cvsn" + (isKeyboard ? " active" : "")
            }
            data-btn="text"
            onClick={bottomControllerClick}
          >
            <i className="fa fa-keyboard"></i>텍스트대화
          </button>
          <button
            type="button"
            className={
              "controller_btn btn_voice_cvsn" + (isMIC ? " active" : "")
            }
            data-btn="voice"
            onClick={bottomControllerClick}
          >
            <i className="fa fa-microphone"></i>음성대화
          </button>
        </div>
      </div>
    </div>
  );
});

export default ChatFooterPage;
