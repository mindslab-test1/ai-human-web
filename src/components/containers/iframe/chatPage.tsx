import { FC, Fragment, useEffect, useState, useRef, memo } from "react";
import { useSelector } from "react-redux";
import LocalStorageUtil from "utils/localStorageUtil";
import { RootState } from "store";
/**
 * reducers
 */
import { Chat } from "reducers/chatBoxs";
/**
 * components
 */
import VideoBox from "components/boxes/iframe/videoBox";
import { StompIframe } from "utils";
/**
 * ChatPage's props
 */
type ChatPageProps = {};

const ChatPage: FC<ChatPageProps> = memo(() => {
  const localStorageUtil: LocalStorageUtil = new LocalStorageUtil();
  /**
   * useState hook variable
   */
  const [isHistory, setIsHistory] = useState(false);
  /**
   * useState hook variable
   */
  const backgroundRef = useRef<HTMLDivElement | null>(null);
  /**
   * reducer variables
   */
  /**
   * get chatBox's reducer
   * we need all of chatBox's variables, so get them all
   */
  const chatBoxState = useSelector(
    (state: RootState) => state.chatBoxs,
    (prev, next) => {
      return (
        JSON.stringify(prev.chatBoxs) === JSON.stringify(next.chatBoxs) && // shallowEqual seems to not compare correctly arrays, so compare them deeply
        prev.currentId === next.currentId &&
        prev.isAddByLocalStorage === next.isAddByLocalStorage
      );
    }
  );
  /**
   * get only background to avoid useless rerendering because of humanIframe's other variables
   */
  const { background } = useSelector(
    (state: RootState) => ({ background: state.humanIframe.background }),
    (prev, next) => {
      return prev.background === next.background;
    }
  );
  /**
   * useEffect functions
   */
  /**
   * if page refreshes, then remove chatting loading from local storage
   */
  useEffect(() => {
    window.addEventListener("beforeunload", () => {
      let data = JSON.parse(localStorageUtil.getLocalStorage() as string);
      let currentId = data["chatBoxs"].length;
      let id = data["chatBoxs"][currentId - 1]["id"];
      let isBot = data["chatBoxs"][currentId - 1]["isBot"];
      if (!isBot && id === currentId)
        localStorageUtil.removeLastDataLocalStorage();
    });
  }, []);
  /**
   * change background when it changes
   */
  useEffect(() => {
    if (background !== undefined) {
      (backgroundRef.current as HTMLDivElement).style.backgroundImage =
        "url(" + background + ")";
    }
  }, [background]);

  return (
    <div className="cont_mid" ref={backgroundRef}>
      <div className="stn">
        <div className="mid_btn_area">
          <button
            type="button"
            className={"btn_avatar " + (!isHistory ? "active" : "")}
            onClick={() => {
              setIsHistory((previsHistory) => false);
            }}
          >
            아바타
          </button>
          <button
            type="button"
            className={"btn_history " + (isHistory ? "active" : "")}
            onClick={() => {
              setIsHistory((previsHistory) => true);
            }}
          >
            히스토리
          </button>
        </div>
        <IsHistoryChat
          isHistory={isHistory}
          chatBoxs={chatBoxState.chatBoxs}
          currentChat={chatBoxState.currentId}
        />
      </div>

      <VideoBox isHistory={isHistory} />
    </div>
  );
});

/**
 * IsHistoryChat's props
 */
interface IsHistoryChatProps {
  isHistory: boolean;
  chatBoxs: Chat[];
  currentChat: number;
}

export const IsHistoryChat: FC<IsHistoryChatProps> = memo(
  ({ isHistory, chatBoxs, currentChat }) => {
    /**
     * reducer variables
     */
    /**
     * get only image to avoid useless rerendering because of humanIframe's other variables
     */
    const { image } = useSelector(
      (state: RootState) => ({ image: state.humanIframe.image }),
      (prev, next) => {
        return prev.image === next.image;
      }
    );
    /**
     * get chatboxstate to check chat hooks
     */
    const chatBoxState = useSelector(
      (state: RootState) => state.chatBoxs,
      (prev, next) => {
        return (
          JSON.stringify(prev.chatBoxs) === JSON.stringify(next.chatBoxs) &&
          prev.currentId === next.currentId &&
          prev.isAddByLocalStorage === next.isAddByLocalStorage
        );
      }
    );
    /**
     * useRef variables
     */
    const chatBoxRef = useRef<HTMLUListElement | null>(null);
    /**
     * useEffect functions
     */
    /**
     * set history tab's list to bottom when user clicked history tab or new chat comes in
     */
    useEffect(() => {
      const onChatAdded = () => {
        chatBoxRef.current?.scrollIntoView({
          behavior: "smooth",
          block: "end",
          inline: "end",
        });
      };
      if (isHistory !== false && chatBoxs !== null) {
        onChatAdded();
      }
    }, [chatBoxs, isHistory]);
    /**
     * if chatbox's curren id changes : new chat comes
     * if new chat is not bot's text, audio, localstorage's chat
     * then send text to api server
     */
    useEffect(() => {
      const currentChat: Chat =
        chatBoxState.chatBoxs[chatBoxState.currentId - 1];

      if (currentChat !== undefined && currentChat.id !== 1) {
        if (
          !currentChat.isBot &&
          !currentChat.isAudio &&
          !chatBoxState.isAddByLocalStorage
        ) {
          StompIframe.sendText(currentChat.text);
        }
      }
    }, [chatBoxState.currentId]);
    return (
      <div className={"scrollBox " + (isHistory ? "history" : "")}>
        {" "}
        {/* change classname with isHistory*/}
        {isHistory ? <p className="info_txt">스크롤을 이동해보세요!</p> : ""}
        <ul className="chat_lst " ref={chatBoxRef}>
          {chatBoxs //return chatboxs only some cases
            .filter((chatBox) => {
              if (!isHistory && chatBox.id === currentChat) {
                // if it is avatar tab and it is last chat (bot answer chat)
                return chatBox;
              } else if (
                // if it is avatar tab and it second of last chat is user's chat (user question chat)
                !isHistory &&
                chatBox.id === currentChat - 1 &&
                !chatBox.isBot
              ) {
                return chatBox;
              } else if (isHistory) {
                // if it is history tap
                return chatBox;
              } else {
                return null;
              }
            })
            .map((chatBox) => (
              <Fragment key={chatBox.id + (isHistory ? "avatar" : "history")}>
                {chatBox.isBot ? (
                  <li className="avatar">
                    <span className="avatar_img">
                      <img alt="아바타" src={image} />
                    </span>
                    <p
                      className="txt"
                      dangerouslySetInnerHTML={{
                        __html: chatBox.text,
                      }}
                    ></p>
                  </li>
                ) : (
                  <Fragment>
                    <li className="question">
                      <span className="txt">{chatBox.text}</span>
                    </li>
                    {!chatBox.isBot &&
                      chatBox.id === currentChat && ( // if chat last chat is user's question and not yet response came then show ... box
                        <li className="avatar loading" value={chatBox.id}>
                          <p className="txt">
                            <span className="lds-ellipsis">
                              <em></em>
                              <em></em>
                              <em></em>
                              <em></em>
                            </span>
                          </p>
                        </li>
                      )}
                  </Fragment>
                )}
              </Fragment>
            ))}
        </ul>
      </div>
    );
  },
  (prevProps, nextProps) => {
    // shallowEqual seems to not compare correctly arrays, so compare them deeply to avoid wrong rerendering
    return (
      JSON.stringify(prevProps.chatBoxs) ===
        JSON.stringify(nextProps.chatBoxs) &&
      prevProps.currentChat === nextProps.currentChat &&
      prevProps.isHistory === nextProps.isHistory
    );
  }
);

export default ChatPage;
