import { useEffect, useState, FC, useCallback, memo } from "react";
import { RootState } from "store";
import { useSelector } from "react-redux";
import { Formatter, StompIframe } from "utils";
/**
 * reducers
 */
import { Chat } from "reducers/chatBoxs";
/**
 * page elements
 */
import ChatHeaderPage from "./chatHeaderPage";
import ChatPage from "./chatPage";
import ChatFooterPage from "./chatFooterPage";
/**
 * hooks
 */
import { useAddChat } from "hook/useChatBoxs";
/**
 * css
 */
import "styles/css/chatbotAvatar.css";

type DisPlayChatProps = {
  sendTextToStomp: (text: string) => void;
};

const DisplayChatMain: FC<DisPlayChatProps> = memo(() => {
  /**
   * useStates
   */
  const [isPcOpenFull, setIsPcOpenFull] = useState(false);
  const [isInited, setIsInited] = useState(false); // to send hello only first of start page
  /**
   * reducer
   */
  const { alert, id } = useSelector(
    (state: RootState) => ({
      alert: state.humanIframe.alert,
      id: state.humanIframe.id,
    }),
    (prev, next) => {
      // we use only 5 case of alert's type, so if type do not matches below arrays, do not rerender
      const usingAlerts = [
        "INIT_DONE",
        "HELLO_DONE",
        "SDS_DONE",
        "STT_DONE",
        "ERROR",
      ];
      if (usingAlerts.includes(prev.alert?.type as string)) {
        // do not rerender when not yet user clicked floating display icon
        return (
          JSON.stringify(prev.alert) === JSON.stringify(next.alert) &&
          prev.id === next.id
        );
      } else return true;
    }
  );
  const { isOpenChat } = useSelector(
    (state: RootState) => ({ isOpenChat: state.saveQuestion.isopenChat }),
    (prev, next) => {
      return prev.isOpenChat === next.isOpenChat;
    }
  );
  /**
   * custom hook objects
   */
  const addChat = useAddChat();
  /**
   * callback functions
   */
  const pcOpenHandler = useCallback(() => {
    const togglePcOpen: boolean = !isPcOpenFull;
    setIsPcOpenFull((prevIsPcOpenFull) => togglePcOpen);
  }, [isPcOpenFull]);

  const addChatList = useCallback(
    (chatText: string, chatIsbot: boolean, date: number, isAudio: boolean) => {
      const chatDate = Formatter.time(date);
      const hookChat: Chat = {
        text: chatText,
        date: chatDate,
        isBot: chatIsbot,
        isAudio: isAudio,
      };

      addChat(hookChat);
    },
    []
  );

  /**
   * useEffects
   */
  /**
   * this is used for send hello to api, when user clicks icon so floating page opened
   * so this useEffect's dependency is managed with only isopenChat which is state of floating page's open or not
   * send hello should be sent only once when user enters page, so alert is not to be considered.
   */
  useEffect(() => {
    if (alert?.type === "INIT_DONE" && !isInited && isOpenChat) {
      // isInited : to send hello only first time , isopenChat : to check user clicked icon so opened floating page
      setIsInited((prevIsInited) => true);
      StompIframe.sendHello();
    }
    return;
  }, [isOpenChat]);
  /**
   * other alerts should be managed with alerts
   * because these alerts may happen several times with various alerts
   */
  useEffect(() => {
    switch (alert?.type) {
      case "HELLO_DONE":
        const helloChat: Chat = {
          text: alert?.message,
          date: "0",
          isBot: true,
          isAudio: true,
        };
        addChat(helloChat);
        return;
      case "SDS_DONE":
        addChatList(alert.message, true, 0, false);
        return;
      case "STT_DONE":
        const sttChat: Chat = {
          text: alert?.message,
          date: "0",
          isBot: false,
          isAudio: true,
        };
        addChat(sttChat);
        return;
      case "ERROR":
        addChatList(alert.message, true, 0, false);
        return;
      default:
        return;
    }
  }, [addChat, addChatList, alert?.type, id]);

  return (
    <div id="chatbotAvatar_wrap">
      <div className="contBox">
        <ChatHeaderPage pcOpenHandler={pcOpenHandler} />
        <ChatPage />
        <ChatFooterPage />
      </div>
    </div>
  );
});

export default DisplayChatMain;
