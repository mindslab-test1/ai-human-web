import { useState, useRef, Fragment, memo } from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
/**
 * components
 */
import styled from "styled-components";
import { Container, StyledUL, StyledList } from "components/Html";
import { BrowserView, MobileView } from "components/View";
/**
 * icons
 */
import IcoQuestion from "assets/images/ico_question.svg";
import { Stomp } from "utils";

export const Recommend = memo(() => {
  const tagWords = useSelector((state: RootState) => state.tagWords);
  const [btnBoxClicked, setbtnBoxClicked] = useState(false);
  const faqIconRef = useRef<HTMLImageElement | null>(null);
  const human = useSelector((state: RootState) => state.human);

  const TransitionButton = styled.button`
    display: inline-block;
    height: 40px;
    margin: 0 16px;
    padding: 0 12px;
    border-radius: 22px;
    background: rgba(65, 72, 83, 0.25);
    color: #fff;
    font-size: 14px;
    font-weight: 500;
    letter-spacing: -0.63px;
    transition: all 0.3s ease-out;

    :hover {
      background: rgba(65, 72, 83, 0.45);
      font-weight: 700;
    }

    @media screen and (max-width: 768px) {
      height: 35px;
      font-size: 10px;
    }
  `;
  return (
    <Fragment>
      <BrowserView>
        <Container
          display={
            human.expectedIntents === null || human.expectedIntents.length === 0
              ? "none"
              : "block"
          }
          className="faq_area"
          position="absolute"
          absoluteTop="50%"
          absoluteLeft="40px"
          width="456px"
          padding="24px"
          boxSizing="border-box"
          transform="translate(0,-50%)"
        >
          <Container className="avatar_msg">
            <span
              style={{
                display: "inline-block",
                width: "52px",
                height: "52px",
                margin: "0 12px 0 0",
                border: "1px solid #eaecee",
                borderRadius: "50%",
                boxSizing: "border-box",
                verticalAlign: "middle",
              }}
            >
              <img
                src={human.image}
                ref={faqIconRef}
                alt="profile"
                style={{
                  width: "100%",
                  height: "100%",
                  objectFit: "contain",
                  verticalAlign: "top",
                  border: "none",
                }}
              ></img>
            </span>
            <p
              className="on"
              style={{
                display: "inline-block",
                padding: "16px",
                borderRadius: "20px 20px 20px 0",
                background: "rgba(65, 72, 83, 0.5)",
                color: "#fff",
                fontSize: "14px",
                fontWeight: 500,
                letterSpacing: "-0.63px",
                verticalAlign: "middle",
              }}
            >
              {btnBoxClicked
                ? "더 자세하게 알아보세요!"
                : "평소 자주 궁금해하시는 질문이나 키워드를 준비했어요!"}
            </p>
          </Container>
          <StyledUL
            style={{
              padding: "24px 0",
            }}
          >
            {tagWords.map((tagword, index) => {
              if (index < 8) {
                return (
                  <StyledList
                    display="inline-block"
                    style={{
                      margin: "0 12px 16px 0",
                    }}
                  >
                    <button
                      className={"question"}
                      onClick={async () => {
                        await Stomp.sendText(tagword.text);
                      }}
                      style={{
                        background:
                          "url(" + IcoQuestion + ") 12px 13px no-repeat",
                      }}
                    >
                      {tagword.text}
                    </button>
                  </StyledList>
                );
              } else {
                return (
                  <StyledList
                    display={btnBoxClicked ? "inline-block" : "none"}
                    style={{
                      margin: "0 12px 16px 0",
                    }}
                    onMouseUp={(e) => {}}
                  >
                    <button
                      className={"question"}
                      onClick={async () => {
                        await Stomp.sendText(tagword.text);
                      }}
                      style={{
                        background:
                          "url(" + IcoQuestion + ") 12px 13px no-repeat",
                      }}
                    >
                      {tagword.text}
                    </button>
                  </StyledList>
                );
              }
            })}
          </StyledUL>

          {tagWords.length > 8 && (
            <Container className="btnBox" textAlign="center">
              <TransitionButton
                onClick={() => {
                  setbtnBoxClicked(!btnBoxClicked);
                }}
              >
                {btnBoxClicked ? "처음으로" : "더보기"}
              </TransitionButton>
            </Container>
          )}
        </Container>
      </BrowserView>
      <MobileView>
        <Container
          display={
            human.expectedIntents === null || human.expectedIntents.length === 0
              ? "none"
              : "block"
          }
          className="faq_area"
          boxSizing="border-box"
          position="absolute"
          transform="translate(-50%, 0)"
          textAlign="center"
          padding="0 20px"
          height="110px"
          width="100%"
          absoluteLeft="50%"
          absoluteBottom="110px"
          absoluteTop="auto"
          style={{
            overflow: "auto",
          }}
        >
          <Container className="avatar_msg" display="none">
            <span
              style={{
                display: "inline-block",
                width: "52px",
                height: "52px",
                margin: "0 12px 0 0",
                border: "1px solid #eaecee",
                borderRadius: "50%",
                boxSizing: "border-box",
                verticalAlign: "middle",
              }}
            >
              <img
                src={human.image}
                alt="profile"
                style={{
                  width: "100%",
                  height: "100%",
                  objectFit: "contain",
                }}
              ></img>
            </span>
            <p
              style={{
                display: "inline-block",
                padding: "16px",
                borderRadius: "20px 20px 20px 0",
                background: "rgba(65, 72, 83, 0.5)",
                color: "#fff",
                fontSize: "14px",
                fontWeight: 500,
                letterSpacing: "-0.63px",
                verticalAlign: "middle",
              }}
            >
              {btnBoxClicked
                ? "더 자세하게 알아보세요!"
                : "평소 자주 궁금해하시는 질문이나 키워드를 준비했어요!"}
            </p>
          </Container>
          <p
            className="avatar_msg_m"
            style={{
              display: "block",
              margin: "0 0 12px",
              color: "#fff",
              fontSize: "10px",
              fontWeight: 500,
              letterSpacing: "-0.45px",
              lineHeight: "1.6",
            }}
          >
            더 많은 컨텐츠를 보시려면 스크롤 하세요!
          </p>
          <StyledUL
            style={{
              padding: "0",
            }}
          >
            {tagWords.map((tagword, index) => {
              if (index < 8) {
                return (
                  <StyledList display="inline-block">
                    <button
                      className={"question"}
                      onClick={async () => {
                        await Stomp.sendText(tagword.text);
                      }}
                      style={{
                        background:
                          "url(" + IcoQuestion + ") 12px 13px no-repeat",
                        backgroundSize: "auto 14px",
                      }}
                    >
                      {tagword.text}
                    </button>
                  </StyledList>
                );
              } else {
                return (
                  <StyledList display={btnBoxClicked ? "inline-block" : "none"}>
                    <button
                      className={"question"}
                      onClick={async () => {
                        await Stomp.sendText(tagword.text);
                      }}
                      style={{
                        background:
                          "url(" + IcoQuestion + ") 12px 13px no-repeat",
                        backgroundSize: "auto 14px",
                      }}
                    >
                      {tagword.text}
                    </button>
                  </StyledList>
                );
              }
            })}
          </StyledUL>
          {tagWords.length > 8 && (
            <Container className="btnBox" textAlign="center">
              <TransitionButton
                onClick={() => {
                  setbtnBoxClicked(!btnBoxClicked);
                }}
              >
                {btnBoxClicked ? "처음으로" : "더보기"}
              </TransitionButton>
            </Container>
          )}
        </Container>
      </MobileView>
    </Fragment>
  );
});
