import styled from "styled-components";

export const ScrollChatContainer = styled.div`
  ::-webkit-scrollbar {
    width: 16px;
    height: 10px;
    border-radius: 0;
  } /* 스크롤바의 width */
  ::-webkit-scrollbar-track {
    background: transparent;
  } /* 스크롤바의 전체 배경색 */
  ::-webkit-scrollbar-thumb {
    border-radius: 2px;
    background-clip: padding-box;
    border-left: 4px solid rgba(255, 255, 255, 0.25);
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    -ms-border-radius: 2px;
    -o-border-radius: 2px;
  } /* 스크롤바 색 */
  ::-webkit-scrollbar-button {
    display: none;
  } /* 위 아래 버튼 (버튼 없애기를 함) */
  overflow-y: auto;
  width: 100%;
  height: calc(100% - 96px);
  padding: 16px 25px 12px 15px;
  box-sizing: border-box;
`;
