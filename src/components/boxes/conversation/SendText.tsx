import React, { FC, memo } from "react";
import { useTheme } from "styled-components";
import { Column, SizedBox } from "components/containers";
import { InputBox } from "components/forms";

import { StyledButton } from "components/Html";
import { IconButton } from "components/buttons";
import IcoClose from "assets/images/ico_close.svg";

interface SendTextProps {
  onClose: React.MouseEventHandler<HTMLElement>;
}
export const SendText: FC<SendTextProps> = memo(({ onClose }) => {
  const theme = useTheme();
  return (
    <Column crossAxisAlignment="center">
      <SizedBox height="24px" />
      <InputBox />
      <SizedBox height="26px" />
      <StyledButton onClick={onClose}>
        <IconButton
          size="36px"
          radius="50%"
          background={theme.background2}
          src={IcoClose}
        />
      </StyledButton>
    </Column>
  );
});
