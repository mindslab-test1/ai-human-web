import React, { FC, useEffect, useRef, memo } from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
import styled from "styled-components";
import { ScrollContainer } from "components/containers";
import { Formatter } from "utils";

import { Container, StyledUL, StyledList } from "components/Html";
import { BodyText1, BodyText2, Caption } from "components/texts";
import ErrorIcon from "assets/images/ico_close.svg";

export enum DebugCode {
  STT = "STT",
  SDS = "SDS",
  AVATAR = "AVATAR",
  ERROR = "ERROR",
}

interface DebugListProps {
  type: DebugCode;
}
const DebugList = styled(StyledList)<DebugListProps>`
  // 동그라미
  :before {
    background: url(${(p) => p.type === DebugCode.ERROR && ErrorIcon});
    background-size: ${(p) => p.type === DebugCode.ERROR && "300%"};
    background-position: ${(p) => p.type === DebugCode.ERROR && "center"};
    filter: ${(p) =>
      p.type === DebugCode.ERROR &&
      "invert(50%) sepia(74%) saturate(2639%) hue-rotate(315deg) brightness(100%) contrast(99%)"};
    --webkit-filter: ${(p) =>
      p.type === DebugCode.ERROR &&
      "invert(50%) sepia(74%) saturate(2639%) hue-rotate(315deg) brightness(100%) contrast(99%)"};
    border: ${(p) => {
      switch (p.type) {
        case DebugCode.STT:
          return "2px solid #fea26a";
        case DebugCode.SDS:
          return "2px solid #8472ff";
        case DebugCode.AVATAR:
          return "2px solid #fd6a70";
        default:
          return;
      }
    }};
    content: "";
    display: inline-block;
    position: absolute;
    top: 4px;
    left: 1px;
    width: 11px;
    height: 11px;
    border-radius: 50%;
    box-sizing: border-box;
  }
  // 선
  :after {
    content: "";
    display: inline-block;
    position: absolute;
    top: 14px;
    left: 5px;
    height: calc(100% + 30px);
    border-left: 1px solid rgba(255, 255, 255, 0.25);
  }
  // 마지막은 선 없음
  :last-child::after {
    content: none;
  }
`;

export const DebugView: FC<{}> = memo(() => {
  const debug = useSelector((state: RootState) => state.human.debug);

  const messageRef = useRef<null | HTMLDivElement>(null);
  useEffect(() => {
    if (messageRef.current) {
      messageRef.current.scrollTop = messageRef.current.scrollHeight;
    }
  }, [debug]);
  return (
    <ScrollContainer ref={messageRef}>
      <StyledUL gap="40px">
        {debug &&
          debug.map((v, idx) => {
            const displayDate =
              v.duration === 0
                ? Formatter.time(v.date)
                : `${v.duration}sec / ${Formatter.time(v.date)}`;
            return (
              <DebugList key={idx} type={v.type}>
                <Container paddingLeft="22px">
                  <BodyText1
                    color="#353b67"
                    fontSize="16px"
                    bold
                    marginBottom="10px"
                  >
                    {v.type}
                  </BodyText1>
                  <BodyText2
                    color="#022b49"
                    fontSize="13px"
                    whiteSpace="pre-line"
                  >
                    {v.debug}
                  </BodyText2>
                  <Caption textAlign="right">{displayDate}</Caption>
                </Container>
              </DebugList>
            );
          })}
      </StyledUL>
    </ScrollContainer>
  );
});
