import React, { FC, useEffect, useRef, memo } from "react";

import { useSelector } from "react-redux";
import { RootState } from "store";
import styled, { useTheme } from "styled-components";
import {
  ScrollContainer,
  Column,
  Row,
  SizedBox,
  ChatMessage,
  Profile,
} from "components/containers";

import { useMediaQuery } from "react-responsive";
import { BrowserView, MobileView, Mobile } from "components/View";
import { StyledList, StyledUL } from "components";
import { BodyText1, Caption } from "components/texts";

import { Formatter } from "utils";

import dompurify from "dompurify";

export enum HistoryCode {
  BOT = "BOT",
  USER = "USER",
  DATE = "DATE",
}

const HistoryContainer = styled.div``;

interface MessageProps {
  right: boolean;
  message: string;
  date: string;
  profile?: string;
}
const BrowserMessage: FC<MessageProps> = memo(
  ({ right, message, date, profile }) => {
    const theme = useTheme();

    return right ? (
      <Row crossAxisAlignment="end" reverse>
        <ChatMessage background={theme.background4} padding="16px" right>
          <BodyText1
            color={right ? undefined : theme.text1}
            dangerouslySetInnerHTML={{
              __html: dompurify.sanitize(message),
            }}
          />
        </ChatMessage>
        <Caption marginRight="12px">{date}</Caption>
      </Row>
    ) : (
      <Row crossAxisAlignment="end">
        <Row crossAxisAlignment="center">
          <Profile
            src={profile}
            size="52px"
            border="1px solid #eaecee"
            paddingRight="52px"
            marginRight="16px"
          />
          <ChatMessage background={theme.background3} padding="16px">
            <BodyText1
              color={theme.text1}
              dangerouslySetInnerHTML={{
                __html: dompurify.sanitize(message),
              }}
            />
          </ChatMessage>
        </Row>
        <Caption marginLeft="12px">{date}</Caption>
      </Row>
    );
  }
);

const MobileMessage: FC<MessageProps> = memo(
  ({ right, message, date, profile }) => {
    const theme = useTheme();

    return right ? (
      <Column crossAxisAlignment="start">
        <ChatMessage background={theme.background4} padding="16px" right>
          <BodyText1
            color={right ? undefined : theme.text1}
            dangerouslySetInnerHTML={{
              __html: dompurify.sanitize(message),
            }}
          />
        </ChatMessage>
        <Caption marginTop="4px">{date}</Caption>
      </Column>
    ) : (
      <Column crossAxisAlignment="end">
        <Row crossAxisAlignment="center">
          <Profile
            src={profile}
            size="28px"
            border="1px solid #eaecee"
            paddingRight="28px"
            marginRight="4px"
          />
          <ChatMessage background={theme.background3} padding="16px">
            <BodyText1
              color={theme.text1}
              dangerouslySetInnerHTML={{
                __html: dompurify.sanitize(message),
              }}
            />
          </ChatMessage>
        </Row>
        <Caption marginTop="4px">{date}</Caption>
      </Column>
    );
  }
);

export const HistoryView = memo(() => {
  const isMobile = useMediaQuery(Mobile);
  const human = useSelector((state: RootState) => state.human);
  const { loading, history, image: profile } = human;

  const messageRef = useRef<null | HTMLDivElement>(null);
  useEffect(() => {
    if (messageRef.current) {
      messageRef.current.scrollTop = messageRef.current.scrollHeight;
    }
  }, [history]);

  return (
    <ScrollContainer ref={messageRef}>
      <HistoryContainer>
        <SizedBox height="24px" />
        <StyledUL gap={isMobile ? "22px" : "40px"}>
          {history &&
            history.map((v: any, idx: number) => {
              const isBot = v.type === HistoryCode.BOT;
              const displayDate = Formatter.time(v.date);

              return (
                <StyledList key={idx} float={isBot ? "left" : "right"}>
                  <BrowserView>
                    <BrowserMessage
                      right={!isBot}
                      message={v.message}
                      date={displayDate}
                      profile={profile}
                    />
                  </BrowserView>
                  <MobileView>
                    <MobileMessage
                      right={!isBot}
                      message={v.message}
                      date={displayDate}
                      profile={profile}
                    />
                  </MobileView>
                </StyledList>
              );
            })}
          {loading ? (
            <StyledList float="left">
              <Row crossAxisAlignment="center">
                <Profile
                  src={profile}
                  size={isMobile ? "28px" : "52px"}
                  border="1px solid #eaecee"
                />
                <SizedBox width={isMobile ? "4px" : "16px"} />
                <Caption color="#f4f6f8">Now Loading…</Caption>
              </Row>
            </StyledList>
          ) : null}
        </StyledUL>
      </HistoryContainer>
    </ScrollContainer>
  );
});
