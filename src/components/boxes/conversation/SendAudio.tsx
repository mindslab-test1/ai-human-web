import React, { FC, useState, memo } from "react";
import { useTheme } from "styled-components";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { Column, SizedBox } from "components/containers";
import IcoMic from "assets/images/ico_mic.svg";
import IcoMicSend from "assets/images/ico_micSend.svg";
import AudioRecorder from "utils/AudioRecorder";
import { useMediaQuery } from "react-responsive";
import { Mobile } from "components/View";

import { Container, StyledButton } from "components/Html";
import { MicButton, IconButton } from "components/buttons";
import { BodyText1 } from "components/texts";

import IcoClose from "assets/images/ico_close.svg";
interface SendAudioProps {
  onClose: React.MouseEventHandler<HTMLElement>;
}
export const SendAudio: FC<SendAudioProps> = memo(({ onClose }) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(Mobile);

  const loading = useSelector((state: RootState) => state.human.loading);
  let _audioRecorder: AudioRecorder = new AudioRecorder();
  const [_isAudioRecording, setisAudioRecording] = useState(false);

  const handleMicButton = () => {
    if (!loading) {
      if (_isAudioRecording) {
        _audioRecorder?.stopRecording(true, true);
        setisAudioRecording(false);
      } else {
        _audioRecorder.startRecording();
        setisAudioRecording(true);
      }
    }
  };

  return (
    <Container
      width="240px"
      padding="24px 0 26px"
      radius="20px"
      background={theme.background2}
    >
      <Column crossAxisAlignment="center">
        <MicButton onClick={handleMicButton}>
          {_isAudioRecording ? (
            <img src={IcoMicSend} width="36px" height="36px" alt="" />
          ) : (
            <img src={IcoMic} alt="" />
          )}
        </MicButton>
        <SizedBox height={isMobile ? "16px" : "8px"} />
        <BodyText1 fontSize="12px" textAlign="center">
          입력이 끝나면 버튼을 클릭해
          <br />
          대화를 완료해주세요.
        </BodyText1>
        <SizedBox height="26px" />

        <StyledButton onClick={onClose}>
          <IconButton
            size="36px"
            radius="50%"
            background={theme.background2}
            src={IcoClose}
          />
        </StyledButton>
      </Column>
    </Container>
  );
});
