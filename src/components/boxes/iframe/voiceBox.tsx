import { useState, FC, memo } from "react";
import MicRecording from "../../buttons/MicRecording";

/**
 * voiceBox's props
 */
type VoiceBoxProps = {
  audioHandler: (isRecord: boolean, isFinish: boolean) => void;
};

export const VoiceBox: FC<VoiceBoxProps> = memo(
  ({ audioHandler }) => {
    /**
     * useState variable
     */
    const [isAudioSended, setIsAudioSended] = useState(false);
    /**
     * onclick function of send audio button
     * icon consist with just icon image and border icon so it have to detect both of them
     * switch's ternary operator means detecting both of them
     * @param e : button event handler
     */
    const sendAuidoClick = (e: any) => {
      let buttonIcon = e.target.parentElement;
      let button = e.target.parentElement.parentElement;
      switch (
        buttonIcon.className === "btn_send"
          ? button.className
          : buttonIcon.className
      ) {
        case "enter_voice_area voice_no_streaming":
          setIsAudioSended((prevIsAudioSended) => true);
          audioHandler(false, true);
          return;
        case "reEnter_voice":
          setIsAudioSended((prevIsAudioSended) => false);
          audioHandler(true, false);
          return;
        default:
      }
    };
    return (
      <div className="crb_cnt">
        <div
          className="enter_voice_area voice_no_streaming"
          style={{ display: isAudioSended ? "none" : "block" }}
        >
          <div className="equalizer">
            <span className="guide_txt">음성을 듣고 있어요!</span>
            <MicRecording /> {/* effect that recording is running */}
          </div>
          <button
            type="button"
            className="btn_send"
            title="전송하기"
            onClick={sendAuidoClick}
          >
            <i className="fa fa-long-arrow-alt-up"></i>전송하기
          </button>
        </div>
        <div
          className="reEnter_voice"
          style={{ display: isAudioSended ? "block" : "none" }}
        >
          <button
            type="button"
            className="btn_reQuestion"
            onClick={sendAuidoClick}
          >
            입력하기
          </button>
        </div>
      </div>
    );
  },
  (prev, next) => {
    return (
      JSON.stringify(prev.audioHandler) === JSON.stringify(next.audioHandler) // to compare deeply
    );
  }
);
