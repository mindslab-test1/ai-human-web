import React, { FC, useEffect, useCallback, useRef, memo } from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { Chat } from "reducers/chatBoxs";
import { useChatBoxs, useAddChat } from "hook/useChatBoxs";
import { ScrollChatContainer } from "components/containers/ScrollChat";

import "styles/css/chatbotAvatar.css";

interface ChatList {
  scrolIcon: string;
  sendTextToStomp: (text: string) => void;
}

const ChatBox: FC<ChatList> = memo(({ scrolIcon, sendTextToStomp }) => {
  const chatBox = useChatBoxs();
  const addChat = useAddChat();

  const chatBoxRef = useRef<HTMLUListElement | null>(null);
  const humanIframe = useSelector((state: RootState) => state.humanIframe);
  const tagWords = useSelector((state: RootState) => state.tagWords);
  const onClickTag = (tag: string) => {
    if (humanIframe.loading === false) {
      const chat: Chat = {
        text: tag,
        date: "",
        isBot: false,
        isAudio: false,
      };
      addChat(chat);
    }
  };

  const onChatAdded = () => {
    chatBoxRef.current?.scrollIntoView({
      behavior: "smooth",
      block: "end",
      inline: "end",
    });
  };

  useEffect(() => {
    onChatAdded();
  }, [chatBox.chatBoxs]);

  return (
    <div className={"scrollBox " + scrolIcon}>
      <p className="info_txt">더 많은 컨텐츠를 보시려면 스크롤 하세요!</p>
      <ul className="faq_lst">
        {tagWords.map((tagWord) => (
          <li className="hashtag" key={tagWord.id}>
            <a
              href={tagWord.path}
              onClick={() => {
                onClickTag(tagWord.text);
              }}
            >
              {tagWord.text}
            </a>
          </li>
        ))}
      </ul>
      <div className="btnBox faq_btn_group">
        <button type="button" className="btn_more_view on">
          더보기
        </button>
        <button type="button" className="btn_go_first">
          처음으로
        </button>
      </div>
      <ul className="chat_lst" ref={chatBoxRef}>
        {chatBox.chatBoxs.map((chat) => (
          <li className={chat.isBot ? "avatar" : "user"} key={chat.id}>
            <p className="txt">{chat.text}</p>
            <span className="date">{chat.date}</span>
          </li>
        ))}
      </ul>
    </div>
  );
});

export default ChatBox;
