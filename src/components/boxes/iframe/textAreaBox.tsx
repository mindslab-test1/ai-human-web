import {
  memo,
  SetStateAction,
  useEffect,
  Dispatch,
  MutableRefObject,
  FC,
  useState,
} from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
/**
 * custom hook
 */
import { useAddChat } from "hook/useChatBoxs";
/**
 * reducer
 */
import { Chat } from "reducers/chatBoxs";
import { StompIframe } from "utils";

/**
 * TextAreaBox's props
 */
type TextAreaBoxProps = {
  setIsTyped: Dispatch<SetStateAction<boolean>>;
  textRef: MutableRefObject<HTMLInputElement | null>;
  loading?: boolean;
};

export const TextAreaBox: FC<TextAreaBoxProps> = memo(
  ({ setIsTyped, textRef, loading }) => {
    /**
     * custom hook object
     */
    const addChat = useAddChat();
    /**
     * useState variable
     */
    const [message, setMessage] = useState("");
    /**
     * useSelector
     */
    /**
     * get autoComplete from humaniframe
     * @param autoComplete : recommended qustions
     */
    const { autoComplete } = useSelector(
      (state: RootState) => ({
        autoComplete: state.humanIframe.autoComplete,
      }),
      (prev, next) => {
        return (
          JSON.stringify(prev.autoComplete) ===
          JSON.stringify(next.autoComplete)
        );
      }
    );
    /**
     * get isMindslab which means this page is used in mindslab's usage
     * this determined with weather project ID is minus or not
     */
    const { isMindslab } = useSelector(
      (state: RootState) => ({ isMindslab: state.isMindslab.isMindslab }),
      (prev, next) => {
        return prev.isMindslab === next.isMindslab;
      }
    );
    /**
     * useEffect
     */
    /**
     * if this page is used in mindslab, then send autocomplete on change of message
     */
    // useEffect(() => {
    //   if (isMindslab) {
    //     const regExp = /^[가-힣a-zA-Z0-9]*$/;
    //     if (message !== "" && regExp.test(message)) {
    //       setIsTyped((prevIsTyped) => true);
    //       StompIframe.sendAutoComplete(message);
    // } else {
    //       setIsTyped((prevIsTyped) => false);
    //     }
    //   }
    // }, [message]);
    /**
     * if new autoComplete array comes, then sort them with score and show only top 5
     */
    useEffect(() => {
      function custonSort(a: any, b: any) {
        if (a.score === b.score) {
          return 0;
        }
        return a.score < b.score ? 1 : -1;
      }
      autoComplete.sort(custonSort);
    }, [autoComplete]);

    /**
     * when user clicks enter, then default enter's workflow("\n") is blocked and change it to send message
     * @param e : textArea event handler
     */
    const onKeyPress = (e: any) => {
      if (e.key === "Enter") {
        e.preventDefault();
        onSendClicked();
      }
    };
    /**
     * when user send message, then check whether message is empty space message or not.
     * if so, do not send message. or add it to chat custom hook
     * when chat added, (chatPage.tsx, 181) send message with chat
     */
    const onSendClicked = () => {
      if (message.replaceAll("\n", "") !== "" && !loading) {
        const chat: Chat = {
          text: message,
          date: "",
          isBot: false,
          isAudio: false,
        };
        addChat(chat);
      }

      setMessage((prevMessage) => "");
      (textRef.current as HTMLInputElement).focus();
    };
    /**
     * when textarea's message changed, change message
     * @param e : textarea event handler
     */
    const onChangeMessage = (e: any) => setMessage(e.target.value);
    /**
     * when user clicks autocomplete button, then make same process as send message
     * @param e : button event handler
     */
    const onAutoCompleteClicked = (e: any) => {
      const chat: Chat = {
        text: e.target.value,
        date: "",
        isBot: false,
        isAudio: false,
      };
      addChat(chat);
      setMessage((prevMessage) => "");
      setIsTyped((prevIsTyped) => false);
      (textRef.current as HTMLInputElement).focus(); // change focus to textarea
    };

    return (
      <div className="crb_cnt">
        {isMindslab && (
          <div className="auto_complete">
            {autoComplete.map((value: any, index: number) => {
              if (index < 5) {
                // show only top 5 score elements
                return (
                  <button
                    type="button"
                    onClick={onAutoCompleteClicked}
                    value={value.text}
                    dangerouslySetInnerHTML={{
                      __html: value.text.replaceAll(
                        message,
                        "<em>" + message + "</em>"
                      ),
                    }}
                  ></button>
                );
              } else return null;
            })}
          </div>
        )}
        <div className="text_area">
          <input
            type="search"
            placeholder="궁금한 점을 물어보세요."
            value={message}
            onChange={onChangeMessage}
            onKeyPress={onKeyPress}
            ref={textRef}
          ></input>
          <button
            type="submit"
            className="btn_send"
            title="전송하기"
            onClick={onSendClicked}
          >
            <i className="fa fa-long-arrow-alt-up"></i>
            전송하기
          </button>
        </div>
      </div>
    );
  },
  (prev, next) => {
    return prev.setIsTyped === next.setIsTyped && prev.textRef === next.textRef;
  }
);
