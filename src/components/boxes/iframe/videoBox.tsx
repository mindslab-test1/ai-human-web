import { useRef, useEffect, useState, FC, memo } from "react";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { RootState } from "store";
import { isIOS, isMobile } from "react-device-detect";

/**
 * redux reducers
 */
import { VideoState } from "reducers/videoElement";
import { clearLipSync } from "reducers/humanIframe";
/**
 * custom hooks
 */
import {
  useVideoElement,
  useSetInitialVideoSrc,
  useSetLoadedVideoSrc,
  useSetInit,
  usePlayVideoElement,
  usePauseVideoElement,
  useTempPauseElement,
  useNotTempPauseElement,
  useMuteVideoElement,
  useUnMuteVideoElement,
} from "hook/useVideoElement";
import { useSaveQuestion } from "hook/useSaveQuestion";

type VideoProps = {
  isHistory: boolean;
};
export const VideoBox: FC<VideoProps> = memo(({ isHistory }) => {
  const dispatch = useDispatch();
  const { videoPose, initialVideoSrc, loadedVideoSrc } = useSelector(
    (state: RootState) => ({
      videoPose: state.humanIframe.pose,
      initialVideoSrc: state.humanIframe.video,
      loadedVideoSrc: state.humanIframe.lipSyncVideo,
    }),
    shallowEqual
  );
  const { isPauseVideo, loadedVideo, initialVideo } = useSelector(
    (state: RootState) => ({
      isPauseVideo: state.videoElement.isPauseVideo,
      loadedVideo: state.videoElement.loadedVideo,
      initialVideo: state.videoElement.initialVideo,
    }),
    (prev, next) => {
      let returnState = true;
      if (prev.isPauseVideo !== undefined)
        returnState = returnState && prev.isPauseVideo === next.isPauseVideo;
      if (prev.initialVideo !== undefined)
        returnState =
          returnState &&
          JSON.stringify(prev.initialVideo) ===
            JSON.stringify(next.initialVideo);
      if (prev.loadedVideo !== undefined)
        returnState =
          returnState &&
          JSON.stringify(prev.loadedVideo) === JSON.stringify(next.loadedVideo);
      return returnState;
    }
  );
  /**
   * ref variables
   */
  let videoRef = useRef<HTMLVideoElement | null>(null);
  let canvasRef = useRef<HTMLCanvasElement | null>(null); // these two refs are let variables because it changes according to videos
  const initialVideoRef = useRef<HTMLVideoElement | null>(null);
  const initialCanvasRef = useRef<HTMLCanvasElement | null>(null);
  const loadedVideoRef = useRef<HTMLVideoElement | null>(null);
  const loadedCanvasRef = useRef<HTMLCanvasElement | null>(null);
  const videoBoxRef = useRef<HTMLDivElement | null>(null);

  /**
   * setState variables
   */
  const [isInit, setIsInit] = useState(true);
  const [toggleVideo, setToggleVideo] = useState(true);
  const [isMute, setIsMute] = useState(isMobile && isIOS ? true : false); // if user uses iphone, it should be muted when initiated
  const [isWholeVideo, setIsWholeVideo] = useState(false);
  /**
   * custom hooks
   */
  const saveQuestion = useSaveQuestion(); // belows are videoElement
  const { isTempPauseVideo } = useVideoElement();
  const setInitialVideoSrc = useSetInitialVideoSrc();
  const setLoadedVideoSrc = useSetLoadedVideoSrc();
  const setInit = useSetInit();
  const playVideoElement = usePlayVideoElement();
  const pauseVideoElement = usePauseVideoElement();
  const tempPauseVideo = useTempPauseElement();
  const notTempPauseVideo = useNotTempPauseElement();
  const pauseVideoRef = useRef(false);
  const muteVideoElement = useMuteVideoElement();
  const unMuteVideoElement = useUnMuteVideoElement();
  /**
   * variables to use chromakey
   */
  let video: HTMLVideoElement | null;
  let c2: HTMLCanvasElement | null;
  let ctx2: CanvasRenderingContext2D | null | undefined;
  let width: number | undefined;
  let height: number | undefined;
  /**
   * start chromakeying
   */
  const doLoad = () => {
    videoRef = isInit ? initialVideoRef : loadedVideoRef;
    canvasRef = isInit ? initialCanvasRef : loadedCanvasRef;
    video = videoRef.current;
    c2 = canvasRef.current;
    ctx2 = c2?.getContext("2d", {
      alpha: true,
      desynchronized: true,
    });

    ctx2?.clearRect(0, 0, c2?.width as number, c2?.height as number);
    (ctx2 as CanvasRenderingContext2D).imageSmoothingEnabled = true;
    (ctx2 as CanvasRenderingContext2D).strokeStyle = "hsl(0,0%,50%)";
    (ctx2 as CanvasRenderingContext2D).setTransform(1, 0, 0, 1, 0.5, 0.5);
    width = c2?.width;
    height = c2?.height;
    processor.timerCallback();
  };

  let processor = {
    timerCallback: function () {
      if (video?.ended) {
        return;
      }
      //video가 일시정지 되는 경우
      if (video?.paused && pauseVideoRef.current) {
        return;
      }
      //video가 다시 로드되는 경우
      else if (video?.paused && !pauseVideoRef.current) {
        loadedVideo?.play();
        return;
      }
      if (!document.hidden) {
        this.computeFrame();
      }
      let self = this;
      setTimeout(function () {
        self.timerCallback();
      }, 30);
    },

    computeFrame: function () {
      ctx2?.drawImage(
        video as HTMLVideoElement,
        0,
        0,
        width as number,
        height as number
      );

      let frame: ImageData | undefined = ctx2?.getImageData(
        0,
        0,
        width as number,
        height as number
      );
      for (let i = 0; i <= (frame as ImageData).data.length; i += 4) {
        let r = (frame as ImageData).data[i];
        let g = (frame as ImageData).data[i + 1];
        let b = (frame as ImageData).data[i + 2];
        if (
          (g > 80 && r < 60 && b < 60) ||
          (g > 110 && r < 90 && b < 90) ||
          (g > 130 && r < 110 && b < 110) ||
          (g > 170 && r < 150 && b < 150) ||
          (g > 190 && r < 170 && b < 170) ||
          (g > 210 && r < 200 && b < 200)
        ) {
          (frame as ImageData).data[i + 3] = 0;
        }
      }
      ctx2?.putImageData(frame as ImageData, 0, 0);

      return;
    },
  };

  //set videoState on initiated
  useEffect(() => {
    const videoState: VideoState = {
      initialVideo: initialVideoRef.current as HTMLVideoElement,
      loadedVideo: loadedVideoRef.current as HTMLVideoElement,
    };
    setInit(videoState);
  }, []);

  /**
   * change video's pose depends on stomp reducer's pose value
   */
  useEffect(() => {
    if (videoPose === "whole") setIsWholeVideo((prevIsWholeVIdeo) => true);
  }, [videoPose]);

  /**
   * change base video when it changed
   */
  useEffect(() => {
    if (initialVideoSrc !== undefined && initialVideoSrc !== null) {
      setInitialVideoSrc(initialVideoSrc);
      initialVideo?.play();
    }
  }, [initialVideoSrc]);

  /**
   * change lipsync video when it changed
   */
  useEffect(() => {
    if (loadedVideoSrc !== undefined && loadedVideoSrc !== null) {
      setLoadedVideoSrc(loadedVideoSrc);
      setIsInit((prevIsInit) => false);
      loadedVideo?.play();
    }
  }, [loadedVideoSrc]);

  /**
   * 채팅을 닫고 열었을 경우 일시 정시 돼있으면 다시 시작
   */
  useEffect(() => {
    if (saveQuestion.isopenChat) {
      if (isTempPauseVideo) {
        playVideoElement();
        notTempPauseVideo();
      }
    } else {
      if ((loadedVideoRef.current as HTMLVideoElement).paused === false) {
        pauseVideoElement();
        tempPauseVideo();
      }
    }
  }, [saveQuestion.isopenChat]);

  /**
   * if user clicks history tab, mute video
   */
  useEffect(() => {
    if (isHistory) {
      (loadedVideoRef.current as HTMLVideoElement).muted = true;
      (videoBoxRef.current as HTMLDivElement).style.display = "none";
    } else {
      (loadedVideoRef.current as HTMLVideoElement).muted = isMute;
      (videoBoxRef.current as HTMLDivElement).style.display = "block";
    }
  }, [isHistory]);

  /**
   * mute lipsyncvideo depend on video element's pause state
   */
  useEffect(() => {
    if (isPauseVideo === undefined || isPauseVideo === null) {
    } else if (isPauseVideo) {
      loadedVideo?.pause();
      pauseVideoRef.current = true;
    } else {
      loadedVideo?.play();
      pauseVideoRef.current = false;
    }
  }, [isPauseVideo]);

  return (
    <div className="videoBox" ref={videoBoxRef}>
      <div
        className={"video " + (isWholeVideo ? "videoWholeBody" : "")}
        style={{ zIndex: 0 }}
      >
        <video
          className="video_base"
          autoPlay
          loop
          muted
          playsInline
          crossOrigin="anonymous"
          ref={initialVideoRef}
          onPlaying={() => {
            if (!toggleVideo) {
              // change toggle video value when it is only false
              setToggleVideo((prevToggleVideo) => !toggleVideo);
            }
            doLoad();
          }}
          onLoadedMetadata={(e) => {
            // when video loaded, set its width, height
            (initialCanvasRef.current as HTMLCanvasElement).width =
              e.currentTarget.videoWidth;
            (initialCanvasRef.current as HTMLCanvasElement).height =
              e.currentTarget.videoHeight;
          }}
        ></video>
        <canvas
          className="video_vid"
          ref={initialCanvasRef}
          style={{
            display: toggleVideo ? "block" : "none",
          }}
        ></canvas>
        <video
          muted={isMute}
          className="video_base"
          playsInline
          crossOrigin="anonymous"
          ref={loadedVideoRef}
          onPlaying={() => {
            if (toggleVideo) {
              // change togglevideo when it is only true
              setToggleVideo((prevToggleVideo) => !toggleVideo);
            }
            (initialVideoRef.current as HTMLVideoElement).pause();
            doLoad();
          }}
          onLoadedMetadata={(e) => {
            (loadedCanvasRef.current as HTMLCanvasElement).width =
              e.currentTarget.videoWidth;
            (loadedCanvasRef.current as HTMLCanvasElement).height =
              e.currentTarget.videoHeight;
          }}
          onEnded={() => {
            // when lipsyncvideo ended change video state to base video
            setIsInit((prevIsInit) => true);
            dispatch(clearLipSync());
            (initialVideoRef.current as HTMLVideoElement).play();
          }}
        ></video>

        <canvas
          className="video_vid"
          ref={loadedCanvasRef}
          style={{
            display: toggleVideo ? "none" : "block",
          }}
        ></canvas>
      </div>
      <div className="video_btn">
        <button
          type="button"
          className={
            "btn_vid btn_pause " + (isPauseVideo ? "btn_vid_play" : "")
          }
          onClick={() => {
            const isPauseVideo = (loadedVideoRef.current as HTMLVideoElement)
              .paused;
            isPauseVideo ? playVideoElement() : pauseVideoElement();
          }}
        >
          <i className="fas fa-pause"></i>
        </button>
        <button
          type="button"
          className={"btn_vid btn_mute " + (isMute ? "on" : "")}
          onClick={() => {
            isMute ? muteVideoElement() : unMuteVideoElement();
            setIsMute(!isMute);
          }}
        >
          <i className="fa"></i>
        </button>
      </div>
    </div>
  );
}, shallowEqual);

export default VideoBox;
