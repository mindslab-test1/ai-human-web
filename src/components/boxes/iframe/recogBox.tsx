import React, { useState, FC, Fragment, memo } from "react";
import voiceIcon from "../../../assets/images/ico_voice_cvsn.svg";
import { Chat } from "reducers/chatBoxs";
import { useAddChat } from "hook/useChatBoxs";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { useMuteVideo, useUnmuteVideo } from "hook/useMuteButton";

type RecogProps = {
  isIconsOn: any[];
  scrollIconHandler: (id: number) => void;
  onChatToggle: () => void;
  sendTextToStomp: (text: string) => void;
};

const RecogBox: FC<RecogProps> = memo(
  ({ isIconsOn, scrollIconHandler, onChatToggle, sendTextToStomp }) => {
    const humanIframe = useSelector((state: RootState) => state.humanIframe);
    const muteVideo = useMuteVideo();
    const unmuteVideo = useUnmuteVideo();
    const addChat = useAddChat();
    const [message, setMessage] = useState("");

    const onChangeMessage = (e: any) => setMessage(e.target.value);
    const onSendMClicked = () => {
      if (message !== "" && !humanIframe.loading) {
        const chat: Chat = {
          text: message,
          date: "",
          isBot: false,
          isAudio: false,
        };
        addChat(chat);
        setMessage("");
      }
    };

    return (
      <Fragment>
        <div
          className={
            "conversationRecogBox voice " +
            (isIconsOn[1].isOn === false ? "" : "on")
          }
        >
          <span className="imgBox">
            <img src={voiceIcon} alt="음성대화 아이콘" />
          </span>
          <p className="guide_txt">
            AI Human이 이야기를 듣고 있습니다. 입력이 끝나면 음성대화를 눌러
            완료해주세요!
          </p>
        </div>

        <div
          className={
            "conversationRecogBox text " +
            (isIconsOn[2].isOn === false ? "" : "on")
          }
        >
          <textarea
            placeholder="대화하실 내용을 입력해주세요."
            value={message}
            onChange={onChangeMessage}
          ></textarea>
          <button
            type="submit"
            className="btn_send"
            title="전송하기"
            onClick={onSendMClicked}
          >
            전송하기
          </button>
        </div>

        <div className="controller">
          <button
            type="button"
            className={"btn_mute" + (isIconsOn[0].isOn === true ? " on" : "")}
            onClick={() => {
              scrollIconHandler(1);
              muteVideo();
            }}
          >
            소리 끄기
          </button>
          <button
            type="button"
            className={
              "btn_mute mute" + (isIconsOn[0].isOn === true ? "" : " on")
            }
            onClick={() => {
              scrollIconHandler(1);
              unmuteVideo();
            }}
          >
            소리 켜기
          </button>
          <button
            type="button"
            className={
              "btn_voice_cvsn " + (isIconsOn[1].isOn === true ? "active" : "")
            }
            onClick={() => {
              scrollIconHandler(2);
            }}
          >
            음성대화
          </button>
          <button
            type="button"
            className={
              "btn_text_cvsn " + (isIconsOn[2].isOn === true ? "active" : "")
            }
            onClick={() => {
              scrollIconHandler(3);
            }}
          >
            텍스트대화
          </button>
          <button type="button" className="btn_setting">
            설정
          </button>
          <button type="button" className="btn_minimize" onClick={onChatToggle}>
            최소화
          </button>
        </div>
      </Fragment>
    );
  }
);

export default RecogBox;
