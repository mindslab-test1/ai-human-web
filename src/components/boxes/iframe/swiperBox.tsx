import { memo, useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
/**
 * import custom hook
 */
import { useAddChat } from "hook/useChatBoxs";
import { useAddTagWordList } from "hook/useTagWords";
/**
 * import redux reducer
 */
import { Chat } from "reducers/chatBoxs";
import { TagWord } from "reducers/tagWord";
/**
 * import swiper library
 */
import { Swiper, SwiperSlide } from "swiper/react";

export const SwiperBox = memo(() => {
  /**
   * custom hook variable
   */
  const addChat = useAddChat();
  const addTagWordList = useAddTagWordList();
  /**
   * useSelector
   */
  /**
   * get tagwordText from tagwords, which we need only tagword's text
   */
  const { tagwordTexts } = useSelector(
    (state: RootState) => ({
      tagwordTexts: state.tagWords.map((value) => {
        return value.text;
      }),
    }),
    (prev, next) => {
      return (
        JSON.stringify(prev.tagwordTexts) === JSON.stringify(next.tagwordTexts)
      ); // compare deeply
    }
  );
  /**
   * get expectedintents and set them to tagword
   */
  const { expectedIntents } = useSelector(
    (state: RootState) => ({
      expectedIntents: state.humanIframe.expectedIntents,
    }),
    (prev, next) => {
      return (
        JSON.stringify(prev.expectedIntents) ===
        JSON.stringify(next.expectedIntents)
      );
    }
  );
  /**
   * onclick function of swiper button
   * add chat with clicked text
   * @param tag : which text user clicked
   */
  const onTagSendClicked = (tag: string) => {
    const chat: Chat = {
      text: tag,
      date: "",
      isBot: false,
      isAudio: false,
    };
    addChat(chat);
  };
  /**
   * useEffect
   */
  /**
   * if expectedIntents changes, then set swiper elements with new one
   */
  useEffect(() => {
    const num: number = expectedIntents.length;
    const obj: {
      id: string;
      text: string;
    }[] = expectedIntents;
    const tags: TagWord[] = [];

    for (let i = 0; i < num; i++) {
      const tag: TagWord = {
        id: i,
        isTag: true,
        path: "#!",
        text: JSON.stringify(obj[i]).replace('"', "").replace('"', ""),
      };
      tags.push(tag);
    }

    addTagWordList(tags);
  }, [expectedIntents]);
  return (
    <div className="crb_cnt">
      <Swiper
        className="recogBoxBtn swiper-container"
        spaceBetween={10}
        slidesPerView={"auto"}
        pagination={{
          clickable: true,
        }}
        observer={true}
        observeParents={true}
        observeSlideChildren={true}
        freeMode={true}
        freeModeSticky={true}
        grabCursor={true}
      >
        {tagwordTexts.map((tagwordText, index) => {
          return (
            <SwiperSlide
              key={index}
              className="swiper-slide"
              style={{
                width: "fit-content",
                height: "100%",
                whiteSpace: "nowrap",
              }}
            >
              <button
                onClick={() => {
                  onTagSendClicked(tagwordText);
                }}
              >
                {tagwordText}
              </button>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
});
