import React, { FC, useState, memo } from "react";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { useTheme } from "styled-components";

import { SizedBox, Row, StyledInput } from "components/containers";

import { Container, StyledButton } from "components/Html";
import { IconButton } from "components/buttons";
import IcoSend from "assets/images/ico_send.svg";
import { Stomp } from "utils";

interface InputBoxProps {
  onClick?: React.MouseEventHandler<HTMLElement>;
}

export const InputBox: FC<InputBoxProps> = memo(() => {
  const theme = useTheme();
  const loading = useSelector((state: RootState) => state.human.loading); //애매한것
  const [text, setText] = useState("");

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
  };
  const handleEnter = async (e: any) => {
    if (text.replace(/^\s+/, "").replace(/^\s+/, "") === "") {
      return;
    } else {
      if (e.key === "Enter") {
        await send();
      }
    }
  };
  const handleSend = async (e: any) => await send();
  const send = async () => {
    if (!loading) {
      setText("");
      if (text.replace(/^\s+/, "").replace(/^\s+/, "") === "") {
        return;
      }
      await Stomp.sendText(text);
    }
  };

  return (
    <Container
      width="280px"
      padding="8px"
      radius="20px"
      background={theme.background2}
    >
      <Row crossAxisAlignment="center">
        <StyledInput
          type="text"
          width="212px"
          height="44px"
          placeholder="대화하실 내용을 입력해주세요"
          value={text}
          onChange={handleInput}
          onKeyDown={handleEnter}
        />
        <SizedBox width="8px" />
        <StyledButton onClick={handleSend}>
          <IconButton
            src={IcoSend}
            size="44px"
            radius="14px"
            iconSize="36px"
            background={theme.button1}
            boxShadow="0 6px 12px 0 rgb(0 0 0 / 10%)"
          />
        </StyledButton>
      </Row>
    </Container>
  );
});
