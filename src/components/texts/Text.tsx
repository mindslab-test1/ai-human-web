import styled from "styled-components";

interface StyledTextProps {
  position?: string;
  display?: string;
  color?: string;
  fontSize?: string;
  fontWeight?: string;
  letterSpacing?: string;
  lineHeight?: string;
  textAlign?: string;
  wordBreak?: string;
  margin?: string;
  marginTop?: string;
  marginRight?: string;
  marginBottom?: string;
  marginLeft?: string;
  verticalAlign?: string;
  bold?: boolean;
  normal?: boolean;
  whiteSpace?: string;
  zIndex?: string;
  maxWidth?: string;
  maxHeight?: string;
}
export const StyledText = styled.p<StyledTextProps>`
  color: ${(p) => (p.color ? p.color : p.theme.palette.white)};
  font-size: ${(p) => p.fontSize};
  font-weight: ${(p) => {
    if (p.bold) return "700";
    else if (p.normal) return "500";

    return p.fontWeight;
  }};
  letter-spacing: ${(p) => p.letterSpacing};
  line-height: ${(p) => p.lineHeight};
  text-align: ${(p) => p.textAlign};
  word-break: ${(p) => p.wordBreak};
  margin: ${(p) => p.margin};
  margin-top: ${(p) => p.marginTop};
  margin-right: ${(p) => p.marginRight};
  margin-bottom: ${(p) => p.marginBottom};
  margin-left: ${(p) => p.marginLeft};
  vertical-align: ${(p) => p.verticalAlign};
  white-space: ${(p) => p.whiteSpace};
  max-width: ${(p) => p.maxWidth};
  max-height: ${(p) => p.maxHeight};
`;
StyledText.defaultProps = {
  wordBreak: "keep-all",
};

export const H1 = styled.h1<StyledTextProps>`
  position: ${(p) => p.position};
  display: ${(p) => p.display};
  z-index: ${(p) => p.zIndex};
`;

export const BodyText1 = styled(StyledText)``;
BodyText1.defaultProps = {
  fontSize: "14px",
  letterSpacing: "-0.6px",
  lineHeight: "1.4",
};
export const BodyText2 = styled(StyledText)``;
BodyText2.defaultProps = {
  fontSize: "12px",
  lineHeight: "1.54",
};
export const MessageText = styled(StyledText)`
  display: inline-block;
  overflow: auto;
  max-height: maxHeight;
  color: #353b67;
  max-width: maxWidth;
  font-size: fontSize;
  letter-spacing: -0.72px;
  line-height: lineHeight;
  word-break: keep-all;
  vertical-align: bottom;
  ::-webkit-scrollbar {
    width: 4px;
    height: 4px;
    border-radius: 0;
  }
  ::-webkit-scrollbar-track {
    background: transparent;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 2px;
    background: rgba(255, 255, 255, 0.4);
  }
  ::-webkit-scrollbar-button {
    display: none;
  }
  a {
    display: none;
  }
`;
MessageText.defaultProps = {};

export const ButtonText1 = styled(StyledText)``;
ButtonText1.defaultProps = {
  fontSize: "16px",
  fontWeight: "700",
  letterSpacing: "-0.72px",
};
export const ButtonText2 = styled(StyledText)`
  font-size: fontSize;
  font-weight: fontWeight;
  letter-spacing: letterSpacing;
`;
ButtonText2.defaultProps = {
  fontSize: "12px",
  fontWeight: "700",
  letterSpacing: "-0.54px",
};
export const Caption = styled(StyledText)``;
Caption.defaultProps = {
  color: "#616c78",
  fontSize: "10px",
  letterSpacing: "-0.4px",
  lineHeight: "1.4",
};
