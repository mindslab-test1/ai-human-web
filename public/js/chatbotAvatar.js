/* MINDsLab. UX/UI Team CSJ 2021.04.20 */

window.addEventListener("message", (e) => {
  var chatbotAvatarOpenBtn = $("#btn_chatbotAvatar"),
    chatbotAvatarUI = $("#chatbotAvatar_wrap");
  if (e.data.message === "moonSquared") {
    chatbotAvatarOpenBtn.addClass("hide");
    chatbotAvatarUI.addClass("open");
    if ($("#chatbotAvatar_wrap").hasClass("pc")) {
      chatbotAvatarUI.addClass("full");
    }
  }
});

$(document).ready(function () {
  // 챗봇 아바타 열기
  var chatbotAvatarOpenBtn = $("#btn_chatbotAvatar"),
    chatbotAvatarUI = $("#chatbotAvatar_wrap");

  chatbotAvatarOpenBtn.on("click", function () {
    window.parent.postMessage({ message: "openChat" }, "*");
    chatbotAvatarOpenBtn.addClass("hide");
    chatbotAvatarUI.addClass("open");
    if (document.referrer.includes("aixart")) {
      if ($("#chatbotAvatar_wrap").hasClass("pc")) {
        chatbotAvatarUI.addClass("full");
      }
    }
  });
  // 최대화 버튼
  $(".btn_full").on("click", function () {
    window.parent.postMessage({ message: "fullScreen" }, "*");
    if ($("#chatbotAvatar_wrap").hasClass("pc")) {
      if ($("#chatbotAvatar_wrap").hasClass("full")) {
        chatbotAvatarUI.removeClass("full");
      } else {
        chatbotAvatarUI.addClass("full");
      }
    }
  });

  // 컨트롤러 기능 - 최소화
  $(".btn_minimize").on("click", function () {
    window.parent.postMessage({ message: "closeChat" }, "*");
    chatbotAvatarUI.removeClass("open full");
    chatbotAvatarOpenBtn.removeClass("hide");
    $(".chat_lst .avatar").removeAttr("style");
  });

  // 컨트롤러 기능 - 텍스트대화
  $(".conversationRecogBox.text textarea").on("keydown", function () {
    $(".conversationRecogBox.text").addClass("on");
  });
  $(".conversationRecogBox.text textarea").on("keyup", function () {
    if (!$(".conversationRecogBox.text textarea").val()) {
      $(".conversationRecogBox.text").removeClass("on");
    }
  });
});

function changeViewType(e) {
  try {
    if (e.data.viewType === "pc") {
      $("#btn_chatbotAvatar").addClass("pc").removeClass("mo");
      $("#chatbotAvatar_wrap").addClass("pc").removeClass("mo");
      if (
        document.referrer.includes("aixart") &&
        $("#chatbotAvatar_wrap").hasClass("open")
      ) {
        $("#chatbotAvatar_wrap").addClass("full");
      }
    } else if (e.data.viewType === "mo") {
      $("#btn_chatbotAvatar").addClass("mo").removeClass("pc");
      $("#chatbotAvatar_wrap").addClass("mo").removeClass("pc");
      if (
        document.referrer.includes("aixart") &&
        $("#chatbotAvatar_wrap").hasClass("open")
      ) {
        $("#chatbotAvatar_wrap").removeClass("full");
      }
    }
  } catch (e) {}
}

window.onload = function () {
  if (document.referrer.includes("aixart")) {
    $("#chatbotAvatar_wrap").addClass("moonSquared");
  }
  var chatbotLinks = document.querySelectorAll(".chatbot_parent_link");
  Array.prototype.forEach.call(chatbotLinks, function (target) {
    target.addEventListener("click", function () {
      var link = target.getAttribute("href");
      parent.document.location = link;
    });
  });

  window.addEventListener("message", changeViewType, false);
  window.parent.postMessage({ message: "requestViewType" }, "*");
};
