/* MINDsLab. UX/UI Team CSJ 2021.04.15 */

function hasScrolled() {
  var wS = (scrollTop =
      document.documentElement.scrollTop || document.body.scrollTop),
    divStn = document.getElementsByClassName("chatbot_access"),
    viewportHeight = document.documentElement.clientHeight,
    scrollH =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight;

  var iframeBody = document
      .getElementById("iframe_chatbotAvatar")
      .contentWindow.document.getElementsByTagName("body")[0],
    //   iframeTxt = document.getElementById("iframe_chatbotAvatar").contentWindow.document.getElementsByTagName("textarea")[0],
    //   iframeBtn = document.getElementById("iframe_chatbotAvatar").contentWindow.document.getElementsByClassName("btn_send")[0],
    iframefloating = document
      .getElementById("iframe_chatbotAvatar")
      .contentWindow.document.getElementById("btn_chatbotAvatar"),
    iframefloating = iframefloating.getElementsByTagName("em")[0];

  for (var i = 0; i < divStn.length; i++) {
    var elm = divStn[i];
    (pos = elm.getBoundingClientRect()),
      (topPerc = (pos.top / viewportHeight) * 100),
      (bottomPerc = (pos.bottom / viewportHeight) * 100),
      (middle = (topPerc + bottomPerc) / 2),
      (inViewport = middle > 20 && middle < 100 - 20);

    if (inViewport) {
      if (elm.className.indexOf("chatbot_access_on") == -1) {
        elm.classList.add("chatbot_access_on");

        var childs = elm.querySelectorAll(".btn_chatbot_accessOn").length > 0;
        if (!childs) {
          var stnName = elm.getAttribute("name");
        } else {
          var stnName = elm
            .querySelectorAll(".btn_chatbot_accessOn")[0]
            .getAttribute("name");
        }
        iframeBody.className = stnName;

        /* demo test script */
        // iframeTxt.value = stnName;
        // iframeBtn.click();
        iframefloating.textContent = stnName;
      }
    } else {
      elm.classList.remove("chatbot_access_on");
    }
  }

  if (wS === 0) {
    divStn[0].classList.add("chatbot_access_on");
    var stnName = divStn[0].getAttribute("name");
    iframeBody.className = stnName;
    /* demo test script */
    // iframeTxt.value = stnName;
    // iframeBtn.click();
  }

  if (wS === scrollH) {
    if (
      divStn[divStn.length - 1].className.indexOf("chatbot_access_on") == -1
    ) {
      divStn[divStn.length - 1].classList.add("chatbot_access_on");
      var stnName = divStn[divStn.length - 1].getAttribute("name");
      iframeBody.className = stnName;
      /* demo test script */
      // iframeTxt.value = stnName;
      // iframeBtn.click();
    }
  }
}

document.getElementById("iframe_chatbotAvatar").onload = function () {
  var wS = (scrollTop =
      document.documentElement.scrollTop || document.body.scrollTop),
    scrollH =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight,
    divStn = document.getElementsByClassName("chatbot_access");

  var iframeBody = document
      .getElementById("iframe_chatbotAvatar")
      .contentWindow.document.getElementsByTagName("body")[0],
    //   iframeTxt = document.getElementById("iframe_chatbotAvatar").contentWindow.document.getElementsByTagName("textarea")[0],
    //   iframeBtn = document.getElementById("iframe_chatbotAvatar").contentWindow.document.getElementsByClassName("btn_send")[0],
    iframefloating = document
      .getElementById("iframe_chatbotAvatar")
      .contentWindow.document.getElementById("btn_chatbotAvatar"),
    iframefloating = iframefloating.getElementsByTagName("em")[0];

  if (wS === 0) {
    divStn[0].classList.add("chatbot_access_on");
    var stnName = divStn[0].getAttribute("name");
    iframeBody.className = stnName;
  }

  if (wS === scrollH) {
    divStn[divStn.length - 1].classList.add("chatbot_access_on");
    var stnName = divStn[divStn.length - 1].getAttribute("name");
    iframeBody.className = stnName;
  }

  var btnClass = document.getElementsByClassName("btn_chatbot_access");
  for (var i = 0; i < btnClass.length; i++) {
    btnClass[i].addEventListener("click", function () {
      var stnName = this.getAttribute("name");
      iframeBody.className = stnName;
      iframefloating.textContent = stnName;
      /* demo test script */
      // iframeTxt.value = stnName;
      // iframeBtn.click();
      // iframefloating.textContent = stnName;
    });
  }
};

document.addEventListener("DOMContentLoaded", function () {
  var didScroll;
  document.addEventListener("scroll", function () {
    didScroll = true;
  });

  setInterval(function () {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 700);
});

$(function () {
  $(".chatbot_access").each(function () {
    var div = $(this);
    div.find(".btn_chatbot_access").on("click", function () {
      div.find(".btn_chatbot_access").removeClass("btn_chatbot_accessOn");
      $(this).addClass("btn_chatbot_accessOn");
    });
  });
});
