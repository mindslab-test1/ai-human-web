function avatarLoad() {
  changeViewType();
  var elString =
    '<div id="chatbotAvatar">\n' +
    '<iframe id="iframe_chatbotAvatar" src="' +
    botUrl +
    '" frameborder="0" framespacing="0" marginwidth="0" marginheight="0" scrolling="no" vspace="0" allow="microphone"></iframe>\n' +
    "</div>";
  if (document.getElementById("chatbotAvatar") === null) {
    var div = document.createElement("div");
    div.innerHTML = elString;
    while (div.children.length > 0) {
      document.body.appendChild(div.children[0]);
    }
  }
  function iframeMsg(e) {
    try {
      var chatbotAvatar = document.getElementById("chatbotAvatar");
      if (e.data.message == "openChat") {
        chatbotAvatar.classList.add("open");
        if (window.location.href.includes("aixart")) {
          if (chatbotAvatar.classList.contains("pc")) {
            chatbotAvatar.style.width = "100%";
            chatbotAvatar.style.height = "100%";
            chatbotAvatar.classList.add("full");
          }
        }
      } else if (e.data.message == "closeChat") {
        chatbotAvatar.classList.remove("open");
        chatbotAvatar.classList.remove("full");
        chatbotAvatar.removeAttribute("style");
      } else if (e.data.message == "fullScreen") {
        if (chatbotAvatar.classList.contains("pc")) {
          if (chatbotAvatar.classList.contains("full")) {
            chatbotAvatar.removeAttribute("style");
            chatbotAvatar.classList.remove("full");
          } else {
            chatbotAvatar.style.width = "100%";
            chatbotAvatar.style.height = "100%";
            chatbotAvatar.classList.add("full");
          }
        }
      } else if (e.data.message == "requestViewType") {
        changeViewType();
      } else if (e.data.message == "moonSquared") {
        chatbotAvatar.classList.add("open");

        if (window.location.href.includes("aixart")) {
          if (chatbotAvatar.classList.contains("pc")) {
            chatbotAvatar.style.width = "100%";
            chatbotAvatar.style.height = "100%";
            chatbotAvatar.classList.add("full");
          }
          document
            .getElementById("iframe_chatbotAvatar")
            .contentWindow.postMessage({ message: "moonSquared" }, "*");
        }
      }
    } catch (e) {
      console.log("iframeMsg error:" + e.message);
    }
  }
  window.addEventListener("message", iframeMsg, false);
  var currentViewType = undefined;
  function changeViewType() {
    try {
      var chatbotAvatar = document.getElementById("chatbotAvatar");
      var iframeAvatar = document.getElementById("iframe_chatbotAvatar");
      if (
        window.parent.innerWidth > 768 &&
        (currentViewType === "mo" || currentViewType === undefined)
      ) {
        currentViewType = "pc";
        chatbotAvatar.classList.add("pc");
        if (
          window.location.href.includes("aixart") &&
          chatbotAvatar.classList.contains("open")
        ) {
          chatbotAvatar.classList.add("full");
          chatbotAvatar.style.width = "100%";
          chatbotAvatar.style.height = "100%";
        }
        chatbotAvatar.classList.remove("mo");
        iframeAvatar.contentWindow.postMessage({ viewType: "pc" }, "*");
      } else if (
        window.parent.innerWidth <= 768 &&
        (currentViewType === "pc" || currentViewType === undefined)
      ) {
        currentViewType = "mo";
        chatbotAvatar.classList.add("mo");
        if (window.location.href.includes("aixart")) {
          chatbotAvatar.classList.remove("full");
        }
        chatbotAvatar.classList.remove("pc");
        iframeAvatar.contentWindow.postMessage({ viewType: "mo" }, "*");
      }
    } catch (e) {
      console.log("setMedia Error:" + e.message);
    }
  }

  window.addEventListener("resize", function (event) {
    changeViewType();
  });
}
window.addEventListener("load", avatarLoad, false);
